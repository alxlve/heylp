//
//  UserDetailView.swift
//  Heylp
//
//  Created by Alexis Lavie on 23/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit
import Cosmos

class UserDetailView: UIView {
    
    var scrollView: UIScrollView
    var refreshControl: UIRefreshControl
    var contentView: UIView
    var headerContainerView: UIView
    var headerLabelsContainerView: UIView
    var pictureImageView: UIImageView
    var firstNameLabel: UILabel
    var lastNameLabel: UILabel
    var usernameLabel: UILabel
    var genderLabel: UILabel
    var informationContainerView: UIView
    var emailLabel: UILabel
    var birthdayLabel: UILabel
    var mapView: MKMapView
    var userAnnotation: MKPointAnnotation?
    var ownerDescriptionLabel: UILabel
    var ownerNoteCosmosView: CosmosView
    var suscriberDescriptionLabel: UILabel
    var suscriberNoteCosmosView: CosmosView
    var ownedServicesButton: UIButton
    var suscribedServicesButton: UIButton
    var accomplishedServicesButton: UIButton
    
    var didSetupConstraints: Bool
    
    init(_ coder: NSCoder? = nil, _ frame: CGRect? = nil) {
        self.scrollView = UIScrollView()
        self.refreshControl = UIRefreshControl()
        self.contentView = UIView()
        self.headerContainerView = UIView()
        self.headerLabelsContainerView = UIView()
        self.pictureImageView = UIImageView()
        self.firstNameLabel = UILabel()
        self.lastNameLabel = UILabel()
        self.usernameLabel = UILabel()
        self.genderLabel = UILabel()
        self.informationContainerView = UIView()
        self.emailLabel = UILabel()
        self.birthdayLabel = UILabel()
        self.mapView = MKMapView()
        self.ownerDescriptionLabel = UILabel()
        self.ownerNoteCosmosView = CosmosView()
        self.suscriberDescriptionLabel = UILabel()
        self.suscriberNoteCosmosView = CosmosView()
        self.ownedServicesButton = UIButton(type: .System)
        self.suscribedServicesButton = UIButton(type: .System)
        self.accomplishedServicesButton = UIButton(type: .System)
        
        self.didSetupConstraints = false
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(frame: frame!)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder, nil)
    }
    
    override required convenience init(frame: CGRect) {
        self.init(nil, frame)
    }
    
    func configure() {
        backgroundColor = UIColor(patternImage: UIImage(named: "grid.png")!)
        
//        scrollView.backgroundColor = UIColor(patternImage: UIImage(named: "grid.png")!)
        scrollView.alwaysBounceVertical = true
        addSubview(scrollView)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        refreshControl.tintColor = UIColor.whiteColor()
        scrollView.addSubview(refreshControl)
        
        scrollView.addSubview(contentView)
        
//        headerContainerView.backgroundColor = UIColor.grayColor()
        headerContainerView.layer.cornerRadius = 5.0
        contentView.addSubview(headerContainerView)
        
        pictureImageView.image = UIImage.getUncachedImage(named: "icon-user-default.png")
        pictureImageView.layer.cornerRadius = 5.0
        pictureImageView.contentMode = UIViewContentMode.ScaleToFill
        pictureImageView.clipsToBounds = true
        pictureImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow - 1, forAxis: .Vertical)
        pictureImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow - 1, forAxis: .Horizontal)

        headerContainerView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.85)
        headerContainerView.addSubview(pictureImageView)
        
//        headerLabelsContainerView.backgroundColor = UIColor.grayColor()
        headerContainerView.addSubview(headerLabelsContainerView)
        
        firstNameLabel.text = "First Name"
        firstNameLabel.font = UIFont.systemFontOfSize(16.0)
        headerLabelsContainerView.addSubview(firstNameLabel)
        
        lastNameLabel.text = "Last Name"
        lastNameLabel.font = UIFont.systemFontOfSize(16.0)
        lastNameLabel.textAlignment = NSTextAlignment.Center
        lastNameLabel.setContentHuggingPriority(UILayoutPriorityDefaultLow - 1, forAxis: .Horizontal)
        headerLabelsContainerView.addSubview(lastNameLabel)
        
        usernameLabel.text = "username"
        usernameLabel.font = UIFont.systemFontOfSize(16.0)
        usernameLabel.setContentHuggingPriority(UILayoutPriorityDefaultLow - 1, forAxis: .Horizontal)
        headerLabelsContainerView.addSubview(usernameLabel)
        
        genderLabel.text = "Gender"
        genderLabel.font = UIFont.systemFontOfSize(16.0)
        genderLabel.setContentHuggingPriority(UILayoutPriorityDefaultLow - 1, forAxis: .Horizontal)
        headerLabelsContainerView.addSubview(genderLabel)
        
//        informationContainerView.backgroundColor = UIColor.grayColor()
        informationContainerView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.8)
        informationContainerView.userInteractionEnabled = true
        contentView.addSubview(informationContainerView)
        
        emailLabel.text = "username@example.com"
        emailLabel.font = UIFont.systemFontOfSize(16.0)
        emailLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(emailLabel)
        
        birthdayLabel.text = "Birthday"
        birthdayLabel.font = UIFont.systemFontOfSize(16.0)
        birthdayLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(birthdayLabel)
        
        informationContainerView.addSubview(mapView)
        
        ownerDescriptionLabel.text = "As service owner:"
        ownerDescriptionLabel.font = UIFont.systemFontOfSize(14.0)
        ownerDescriptionLabel.textAlignment = NSTextAlignment.Center
        ownerDescriptionLabel.textColor = UIColor.darkGrayColor()
        informationContainerView.addSubview(ownerDescriptionLabel)
        
        ownerNoteCosmosView.rating = 0
        ownerNoteCosmosView.text = "(x)"
        ownerNoteCosmosView.settings.fillMode = .Precise
        ownerNoteCosmosView.settings.starSize = 30
        ownerNoteCosmosView.settings.updateOnTouch = false
        informationContainerView.addSubview(ownerNoteCosmosView)
        
        suscriberDescriptionLabel.text = "As service suscriber:"
        suscriberDescriptionLabel.font = UIFont.systemFontOfSize(14.0)
        suscriberDescriptionLabel.textAlignment = NSTextAlignment.Center
        suscriberDescriptionLabel.textColor = UIColor.darkGrayColor()
        informationContainerView.addSubview(suscriberDescriptionLabel)
        
        suscriberNoteCosmosView.rating = 0
        suscriberNoteCosmosView.text = "(y)"
        suscriberNoteCosmosView.settings.fillMode = .Precise
        suscriberNoteCosmosView.settings.starSize = 30
        suscriberNoteCosmosView.settings.updateOnTouch = false
        informationContainerView.addSubview(suscriberNoteCosmosView)
        
        ownedServicesButton.setTitle("Proposed services", forState: .Normal)
//        ownedServicesButton.tintColor = UIColor.orangeColor()
        informationContainerView.addSubview(ownedServicesButton)
        
        suscribedServicesButton.setTitle("Accepted services", forState: .Normal)
        informationContainerView.addSubview(suscribedServicesButton)
        
        accomplishedServicesButton.setTitle("Accomplished services", forState: .Normal)
        informationContainerView.addSubview(accomplishedServicesButton)
    }
    
    func configureConstraints() {
        if (!didSetupConstraints) {
            scrollView.autoPinEdgesToSuperviewEdges()
            
            contentView.autoPinEdgesToSuperviewEdges()
            contentView.autoMatchDimension(.Width, toDimension: .Width, ofView: scrollView)
            
            let leadingTrailingHeaderContainerViewOffset: CGFloat = 8.0
            let pictureOffset: CGFloat = 3.0
            let topHeaderLabelsContainerViewOffset: CGFloat = 8.0
            let leadingHeaderLabelsContainerViewOffset: CGFloat = 8.0
            let trailingHeaderLabelsContainerViewOffset: CGFloat = 8.0
            let bottomHeaderLabelsContainerViewOffset: CGFloat = 8.0
            let headerHorizontalLabelsSeparatorOffset: CGFloat = 7.0
            let headerVerticalLabelsSeparatorOffset: CGFloat = 9.0
            
            headerContainerView.autoAlignAxisToSuperviewAxis(.Vertical)
            headerContainerView.autoPinEdgeToSuperviewEdge(.Top, withInset: 15.0)
            headerContainerView.autoPinEdgeToSuperviewEdge(.Leading, withInset: leadingTrailingHeaderContainerViewOffset)
            headerContainerView.autoPinEdgeToSuperviewEdge(.Trailing, withInset: leadingTrailingHeaderContainerViewOffset)

            pictureImageView.autoMatchDimension(.Width, toDimension: .Height, ofView: pictureImageView)
            pictureImageView.autoPinEdge(.Top, toEdge: .Top, ofView: headerContainerView, withOffset: pictureOffset)
            pictureImageView.autoPinEdge(.Leading, toEdge: .Leading, ofView: headerContainerView, withOffset: pictureOffset)
            pictureImageView.autoPinEdge(.Trailing, toEdge: .Leading, ofView: headerLabelsContainerView, withOffset: -pictureOffset)
            pictureImageView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: headerContainerView, withOffset: -pictureOffset)
            
            headerLabelsContainerView.autoPinEdge(.Top, toEdge: .Top, ofView: headerContainerView)
            headerLabelsContainerView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: headerContainerView)
            headerLabelsContainerView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: headerContainerView)
            
            firstNameLabel.autoPinEdge(.Top, toEdge: .Top, ofView: headerLabelsContainerView, withOffset: topHeaderLabelsContainerViewOffset)
            firstNameLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: headerLabelsContainerView, withOffset: leadingHeaderLabelsContainerViewOffset)
            
            lastNameLabel.autoPinEdge(.Top, toEdge: .Top, ofView: firstNameLabel)
            lastNameLabel.autoPinEdge(.Leading, toEdge: .Trailing, ofView: firstNameLabel, withOffset: headerHorizontalLabelsSeparatorOffset)
            headerLabelsContainerView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: lastNameLabel, withOffset: trailingHeaderLabelsContainerViewOffset)
            
            usernameLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: firstNameLabel, withOffset: headerVerticalLabelsSeparatorOffset)
            usernameLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: headerLabelsContainerView, withOffset: leadingHeaderLabelsContainerViewOffset)
            headerLabelsContainerView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: usernameLabel, withOffset: trailingHeaderLabelsContainerViewOffset)
            
            genderLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: usernameLabel, withOffset: headerVerticalLabelsSeparatorOffset)
            genderLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: headerLabelsContainerView, withOffset: leadingHeaderLabelsContainerViewOffset)
            headerLabelsContainerView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: genderLabel, withOffset: trailingHeaderLabelsContainerViewOffset)
            headerLabelsContainerView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: genderLabel, withOffset: bottomHeaderLabelsContainerViewOffset)

            let leadingTrailingInformationContainerViewOffset: CGFloat = 16.0
            let topInformationContainerViewOffset: CGFloat = 8.0
            let leadingInformationContainerViewOffset: CGFloat = 8.0
            let trailingInformationContainerViewOffset: CGFloat = 8.0
            let bottomInformationContainerViewOffset: CGFloat = 8.0
            let bottomDownInformationContainerViewOffset: CGFloat = 15.0
            let informationVerticalSeparatorOffset: CGFloat = 9.0
            
            informationContainerView.autoPinEdge(.Top, toEdge: .Bottom, ofView: headerContainerView, withOffset: -1)
            informationContainerView.autoPinEdge(.Leading, toEdge: .Leading, ofView: headerContainerView, withOffset: leadingTrailingInformationContainerViewOffset)
            informationContainerView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: headerContainerView, withOffset: -leadingTrailingInformationContainerViewOffset)
            
            emailLabel.autoPinEdge(.Top, toEdge: .Top, ofView: informationContainerView, withOffset: topInformationContainerViewOffset * 2)
            emailLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            emailLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            birthdayLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: emailLabel, withOffset: informationVerticalSeparatorOffset)
            birthdayLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            birthdayLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            mapView.autoMatchDimension(.Height, toDimension: .Width, ofView: contentView, withMultiplier: 0.5)
            mapView.autoPinEdge(.Top, toEdge: .Bottom, ofView: birthdayLabel, withOffset: informationVerticalSeparatorOffset * 2)
            mapView.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            mapView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            ownerDescriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: mapView, withOffset: informationVerticalSeparatorOffset * 2)
            ownerDescriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            ownerDescriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            ownerNoteCosmosView.autoPinEdge(.Top, toEdge: .Bottom, ofView: ownerDescriptionLabel, withOffset: informationVerticalSeparatorOffset)
            ownerNoteCosmosView.autoAlignAxis(.Vertical, toSameAxisOfView: informationContainerView)
//            ownerNoteCosmosView.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
//            ownerNoteCosmosView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            suscriberDescriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: ownerNoteCosmosView, withOffset: informationVerticalSeparatorOffset * 2)
            suscriberDescriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            suscriberDescriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            suscriberNoteCosmosView.autoPinEdge(.Top, toEdge: .Bottom, ofView: suscriberDescriptionLabel, withOffset: informationVerticalSeparatorOffset)
            suscriberNoteCosmosView.autoAlignAxis(.Vertical, toSameAxisOfView: informationContainerView)
//            suscriberNoteCosmosView.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
//            suscriberNoteCosmosView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)

            ownedServicesButton.autoPinEdge(.Top, toEdge: .Bottom, ofView: suscriberNoteCosmosView, withOffset: informationVerticalSeparatorOffset * 2)
            ownedServicesButton.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            ownedServicesButton.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            suscribedServicesButton.autoPinEdge(.Top, toEdge: .Bottom, ofView: ownedServicesButton, withOffset: informationVerticalSeparatorOffset)
            suscribedServicesButton.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            suscribedServicesButton.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            accomplishedServicesButton.autoPinEdge(.Top, toEdge: .Bottom, ofView: suscribedServicesButton, withOffset: informationVerticalSeparatorOffset)
            accomplishedServicesButton.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            accomplishedServicesButton.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            informationContainerView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: accomplishedServicesButton, withOffset: bottomInformationContainerViewOffset)
            informationContainerView.autoPinEdgeToSuperviewEdge(.Bottom, withInset: bottomDownInformationContainerViewOffset)
            
            didSetupConstraints = true
        }
    }
    
    func changeUserAnnotation(userAnnotation: MKPointAnnotation) {
        if (self.userAnnotation != nil) {
            mapView.removeAnnotation(self.userAnnotation!)
        }
        self.userAnnotation = userAnnotation
        
        mapView.addAnnotation(self.userAnnotation!)
        
        let latitudeDelta: CLLocationDegrees = 0.025
        let longitudeDelta: CLLocationDegrees = 0.025
        let span: MKCoordinateSpan = MKCoordinateSpanMake(latitudeDelta, longitudeDelta)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(self.userAnnotation!.coordinate, span)
        mapView.setRegion(region, animated: false)
        
        mapView.selectAnnotation(self.userAnnotation!, animated: true)
    }
}
