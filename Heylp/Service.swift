//
//  Service.swift
//  Heylp
//
//  Created by Alexis Lavie on 05/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

enum ServiceCategory: String, CustomStringConvertible {
    case Babysitting
    case Beauty
    case Computer
    case Delivery
    case Do_it_yourself
    case Events
    case Gardening
    case Homework_assistance
    case Housekeeping
    case Pets
    case Other
    
    var description: String {
        switch self {
            case .Babysitting: return "Babysitting"
            case .Beauty: return "Beauty / Well-being"
            case .Computer: return "Computer"
            case .Delivery: return "Delivery / Move"
            case .Do_it_yourself: return "Do-It-Yourself"
            case .Events: return "Events"
            case .Gardening: return "Gardening"
            case .Homework_assistance: return "Homework assistance"
            case .Housekeeping: return "Housekeeping"
            case .Pets: return "Pets"
            case .Other: return "Other"
        }
    }
    
    static func values() -> [ServiceCategory] {
        return [.Babysitting, .Beauty, .Computer, .Delivery, .Do_it_yourself, .Events, .Gardening, .Homework_assistance, .Housekeeping, .Pets, .Other]
    }
}

enum ServiceStatus: String, CustomStringConvertible {
    case Available
    case Affected
    case Accomplished
    case Canceled
    
    var description: String {
        switch self {
            case .Available: return "Available"
            case .Affected: return "Affected"
            case .Accomplished: return "Accomplished"
            case .Canceled: return "Canceled"
        }
    }
    
    static func values() -> [ServiceStatus] {
        return [.Available, .Affected, .Accomplished, .Canceled]
    }
}

class Service: Mappable {

    var id: UInt?
    var owner: UInt?
    var suscriber: UInt?
    var category: ServiceCategory!
    var title: String!
    var description: String!
    var picture: Picture?
    var address: Address!
    var price: Float!
    var startDate: NSDate!
    var endDate: NSDate!
    var creationDate: NSDate!
    var status: ServiceStatus!
    var ownerNote: Float?
    var suscriberNote: Float?
    
    init(category: ServiceCategory, title: String, description: String, address: Address, price: Float, startDate: NSDate, endDate: NSDate, creationDate: NSDate, status: ServiceStatus) {
        self.id = nil
        self.category = category
        self.title = title
        self.description = description
        self.address = address
        self.price = price
        self.startDate = startDate
        self.endDate = endDate
        self.creationDate = creationDate
        self.status = status
    }
    
    init() {}
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id             <- (map["id"], NoTransform(serialization: false))
        self.owner          <- (map["owner"], NoTransform(serialization: false))
        self.suscriber      <- (map["suscriber"], NoTransform(serialization: true))
        self.category       <- (map["category"], EnumTransform())
        self.title          <- map["title"]
        self.description    <- map["description"]
//        self.picture        <- map["picture"]
        self.address        <- map["address"]
        self.price          <- map["price"]
        self.startDate      <- (map["startDate"], JavaDateTransform())
        self.endDate        <- (map["endDate"], JavaDateTransform())
        self.creationDate   <- (map["creationDate"], JavaDateTransform())
        self.status         <- (map["status"], EnumTransform())
        self.ownerNote      <- map["ownerNote"]
        self.suscriberNote  <- map["suscriberNote"]
    }
}
