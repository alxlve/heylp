//
//  UIColor.swift
//  Heylp
//
//  Created by Alexis Lavie on 12/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func formerColor() -> UIColor {
        return UIColor(red: 0.14, green: 0.16, blue: 0.22, alpha: 1)
    }
    
    class func formerSubColor() -> UIColor {
        return UIColor(red: 0.9, green: 0.55, blue: 0.08, alpha: 1)
    }
    
    class func formerHighlightedSubColor() -> UIColor {        
        return UIColor(red: 1, green: 0.7, blue: 0.12, alpha: 1)
    }
}
