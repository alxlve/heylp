//
//  ModelAPIProtocol.swift
//  Heylp
//
//  Created by Alexis Lavie on 05/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

protocol ModelAPIProtocol {
    typealias T
    
    func get(id: UInt, completionHandler: (responseObject: T?) -> ())
//    func get(ids: UInt...) -> [T]
    func getAll(completionHandler completionHandler: (responseObject: [T]) -> ())
    func add(item: T, completionHandler: (responseObject: UInt?) -> ())
//    func add(items: T...) -> [UInt?]
    func save(id: UInt, object: T, completion: () -> ())
//    func save(items: T...)
    func delete(item: T)
//    func delete(items: T...)
}
