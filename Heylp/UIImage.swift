//
//  UIImage.swift
//  Heylp
//
//  Created by Alexis Lavie on 26/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

extension UIImage {
    
    class func getUncachedImage(named name: String) -> UIImage? {
        if let imgPath = NSBundle.mainBundle().pathForResource(name, ofType: nil) {
            return UIImage(contentsOfFile: imgPath)
        }
        
        return nil
    }
}
