//
//  App.swift
//  Heylp
//
//  Created by Alexis Lavie on 21/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

class App {
    
    static var sharedInstance = App()

    private var connectedUser: User?
    
    private init() {

    }
    
    func getConnectedUser() -> User? {
        return connectedUser
    }
    
    func setConnectedUser(userId: UInt, completion: (() -> ())?) {
        ModelAPI.sharedInstance.getUser(userId, completionHandler: { (user) in
            self.connectedUser = user
            completion?()
        })
    }
}
