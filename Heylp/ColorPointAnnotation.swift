//
//  ColorPointAnnotation.swift
//  Heylp
//
//  Created by Alexis Lavie on 22/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit

class ColorPointAnnotation: MKPointAnnotation {
    
    var pinColor: UIColor
    
    init(pinColor: UIColor) {
        self.pinColor = pinColor
        
        super.init()
    }
}
