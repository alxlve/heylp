//
//  LoginViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 25/02/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import EasyAnimation
import PureLayout

class LoginViewController: UIViewController {
    
    var action: String
    
    var animationsAreRunning: Bool
    
    var myView: LoginView! {
        return self.view as! LoginView
    }
    
    init(_ coder: NSCoder? = nil) {
        animationsAreRunning = false
        action = "signUp"
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    func textFieldsDidChange() {
        if ((myView.usernameTextField.text != "") && (myView.passwordTextField.text != "")) {
            action = "login"
            myView.actionButton.setTitle("Login", forState: .Normal)
        } else {
            action = "signUp"
            myView.actionButton.setTitle("Sign Up", forState: .Normal)
        }
    }
    
    func guestButtonTouchUpInside(sender: UIButton) {
        let homeTabBarController = HomeTabBarController()
        self.presentViewController(homeTabBarController, animated: true, completion: nil)
    }
    
    func actionButtonTouchUpInside(sender: UIButton) {
        if (action == "signUp") {
            let navigationController = UINavigationController()
            let signUpViewController = SignUpViewController()
        
            navigationController.viewControllers = [signUpViewController]
            presentViewController(navigationController, animated: true, completion: nil)
        } else if (action == "login") {
            let userId = ModelAPI.sharedInstance.login(myView.usernameTextField.text!, password: myView.passwordTextField.text!)
            
            if (userId == nil) {
                let alertController = UIAlertController(title: "Server is offline", message: "Launch the server first !", preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                presentViewController(alertController, animated: true, completion: nil)
            } else {
                if (userId == 0) {
                    let alertController = UIAlertController(title: "We could not log you in", message: "Either the username or password is wrong !", preferredStyle: .Alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    presentViewController(alertController, animated: true, completion: nil)
                } else {
                    App.sharedInstance.setConnectedUser(userId!, completion: {
                        let homeTabBarController = HomeTabBarController()
                        self.presentViewController(homeTabBarController, animated: true, completion: nil)
                    })
                }
            }
        }
    }
    
    override func loadView() {
        super.loadView()
        view = LoginView(frame: UIScreen.mainScreen().bounds)
        
        myView.configure()
        myView.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        myView.configureConstraints()

        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // POUR TESTER - À SUPPRIMER
//        let address: Address = Address(streetNumber: "1", streetName: "rue Tudelle", city: "Orléans", postalCode: "45100", country: "France")
//        address.updateCoordinates(completion: {
//            let image: UIImage? = UIImage(named: "heylp-60@3x.png")
//            let user: User = User(username: "j.d", password: "123", email: "a@b.com", firstName: "Jean", lastName: "Dupont", gender: .Male, birthday: NSDate(), address: address, phones: nil, acceptedServices: nil, proposedServices: nil)
//            ModelAPI.sharedInstance.addUser(user, completionHandler: {
//                ModelAPI.sharedInstance.addUserPicture(user, picture: Picture(image: image!), completion: {
//                    let address: Address = Address(streetNumber: "298", streetName: "rue de Bourgogne", city: "Orléans", postalCode: "45000", country: "France")
//                    address.updateCoordinates(completion: {
//                        let service: Service = Service(category: ServiceCategory.Computer, title: "Réparation d'ordinateurs", description: "Je répare tout...", address: address, price: 10.5, startDate: NSDate(), endDate: NSDate(), creationDate: NSDate(), status: ServiceStatus.Available)
//                        ModelAPI.sharedInstance.addUserProposedService(user, service: service, completionHandler: {
//                            let imageS: UIImage? = UIImage(named: "backgrounds/babysitting.jpg")
//                            
//                            ModelAPI.sharedInstance.addServicePicture(user.id!, service: service, picture: Picture(image: imageS!), completion: nil)
//                        })
//                    })
//                })
//            })
//        })
        // POUR TESTER - À SUPPRIMER
        
        // Delegates
        myView.usernameTextField.delegate = self
        myView.passwordTextField.delegate = self
        
        // Targets
        myView.usernameTextField.addTarget(self, action: "textFieldsDidChange", forControlEvents: UIControlEvents.EditingChanged)
        myView.passwordTextField.addTarget(self, action: "textFieldsDidChange", forControlEvents: UIControlEvents.EditingChanged)
        myView.guestButton.addTarget(self, action: "guestButtonTouchUpInside:", forControlEvents: .TouchUpInside)
        myView.actionButton.addTarget(self, action: "actionButtonTouchUpInside:", forControlEvents: .TouchUpInside)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Keyboard
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillChangeFrame:", name: UIKeyboardWillChangeFrameNotification, object: nil)
        
        myView.changeActiveBackgroundImage(animated: true)
        runAnimations()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Keyboard
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidChangeFrameNotification, object: nil)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationDidBecomeActive:", name: UIApplicationDidBecomeActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillEnterForeground:", name: UIApplicationWillEnterForegroundNotification, object: nil)
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        stopAnimations()
    }

    func applicationDidBecomeActive(notification: NSNotification) {
        runAnimations()
    }

    func applicationWillEnterForeground(notification: NSNotification) {
        stopAnimations()
        myView.changeActiveBackgroundImage(animated: true)
    }
    
    func runAnimations() {
        if (!animationsAreRunning) {
            animationsAreRunning = !animationsAreRunning
            myView.runBackgroundAnimation()
        }
    }

    func stopAnimations() {
        myView.stopBackgroundAnimation()
        animationsAreRunning = false
    }
    
    func keyboardWillShow(notification: NSNotification) {
        myView.adjustlowestBottomConstraint(true, notification: notification)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        myView.adjustlowestBottomConstraint(false, notification: notification)
    }
    
    func keyboardWillChangeFrame(notification: NSNotification) {
        if (myView.lowestBottomConstraintWithKeyboardShown?.active == true) {
            myView.adjustlowestBottomConstraint(true, notification: notification)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        view.endEditing(true)
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {        
        return UIInterfaceOrientationMask.Portrait
    }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if let _ = textField.nextField?.becomeFirstResponder() {
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}
