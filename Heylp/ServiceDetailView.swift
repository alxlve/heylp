//
//  ServiceDetailView.swift
//  Heylp
//
//  Created by Alexis Lavie on 21/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit
import Cosmos

class ServiceDetailView: UIView {
    
    var scrollView: UIScrollView
    var refreshControl: UIRefreshControl
    var contentView: UIView
    var pictureImageView: UIImageView
    var informationContainerView: UIView
    var titleLabel: UILabel
    var categoryLabel: UILabel
    var priceLabel: UILabel
    var descriptionLabel: UILabel
    var mapView: MKMapView
    var serviceAnnotation: MKPointAnnotation?
    var startDateDescriptionLabel: UILabel
    var startDateLabel: UILabel
    var endDateDescriptionLabel: UILabel
    var endDateLabel: UILabel
    var creationDateDescriptionLabel: UILabel
    var creationDateLabel: UILabel
    var statusLabel: UILabel
    var actionButton: UIButton
    var ownerDescriptionLabel: UILabel
    var ownerNoteCosmosView: CosmosView
    var suscriberDescriptionLabel: UILabel
    var suscriberNoteCosmosView: CosmosView
    var ownerButton: UIButton
    var ownerUsernameLabel: UILabel
    var suscriberButton: UIButton
    var suscriberUsernameLabel: UILabel
    
    var didSetupConstraints: Bool
    
    init(_ coder: NSCoder? = nil, _ frame: CGRect? = nil) {
        self.scrollView = UIScrollView()
        self.refreshControl = UIRefreshControl()
        self.contentView = UIView()
        self.pictureImageView = UIImageView()
        self.informationContainerView = UIView()
        self.titleLabel = UILabel()
        self.categoryLabel = UILabel()
        self.priceLabel = UILabel()
        self.descriptionLabel = UILabel()
        self.mapView = MKMapView()
        self.startDateDescriptionLabel = UILabel()
        self.startDateLabel = UILabel()
        self.endDateDescriptionLabel = UILabel()
        self.endDateLabel = UILabel()
        self.creationDateDescriptionLabel = UILabel()
        self.creationDateLabel = UILabel()
        self.statusLabel = UILabel()
        self.actionButton = UIButton(type: UIButtonType.System)
        self.ownerDescriptionLabel = UILabel()
        self.ownerNoteCosmosView = CosmosView()
        self.suscriberDescriptionLabel = UILabel()
        self.suscriberNoteCosmosView = CosmosView()
        self.ownerButton = UIButton(type: UIButtonType.System)
        self.ownerUsernameLabel = UILabel()
        self.suscriberButton = UIButton(type: UIButtonType.System)
        self.suscriberUsernameLabel = UILabel()
        
        self.didSetupConstraints = false
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(frame: frame!)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder, nil)
    }
    
    override required convenience init(frame: CGRect) {
        self.init(nil, frame)
    }
    
    func configure() {
        backgroundColor = UIColor(patternImage: UIImage(named: "grid.png")!)

//        scrollView.backgroundColor = UIColor(patternImage: UIImage(named: "grid.png")!)
        scrollView.alwaysBounceVertical = true
        addSubview(scrollView)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        refreshControl.tintColor = UIColor.whiteColor()
        scrollView.addSubview(refreshControl)
        
        scrollView.addSubview(contentView)
        
//        pictureImageView.image = UIImage.getUncachedImage(named: "backgrounds/computer.jpg")
        pictureImageView.contentMode = UIViewContentMode.ScaleAspectFill
        pictureImageView.clipsToBounds = true
        contentView.addSubview(pictureImageView)
        
        informationContainerView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.85)
        informationContainerView.userInteractionEnabled = true
        contentView.addSubview(informationContainerView)
        
        titleLabel.text = "Title"
        titleLabel.font = UIFont.systemFontOfSize(17.0)
        titleLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(titleLabel)
        
        categoryLabel.text = "Category"
        categoryLabel.font = UIFont.systemFontOfSize(16.0)
        categoryLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(categoryLabel)
        
        priceLabel.text = "--,--€"
        priceLabel.font = UIFont.systemFontOfSize(16.0)
        priceLabel.textAlignment = NSTextAlignment.Right
        informationContainerView.addSubview(priceLabel)
        
        descriptionLabel.text = "Description"
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = UIFont.systemFontOfSize(15.0)
        descriptionLabel.textAlignment = NSTextAlignment.Justified
        informationContainerView.addSubview(descriptionLabel)
        
        informationContainerView.addSubview(mapView)
        
        startDateDescriptionLabel.text = "Start date:"
        startDateDescriptionLabel.font = UIFont.systemFontOfSize(14.0)
        startDateDescriptionLabel.textAlignment = NSTextAlignment.Center
        startDateDescriptionLabel.textColor = UIColor.darkGrayColor()
        informationContainerView.addSubview(startDateDescriptionLabel)
        
        startDateLabel.text = "Start date"
        startDateLabel.font = UIFont.systemFontOfSize(16.0)
        startDateLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(startDateLabel)
        
        endDateDescriptionLabel.text = "End date:"
        endDateDescriptionLabel.font = UIFont.systemFontOfSize(14.0)
        endDateDescriptionLabel.textAlignment = NSTextAlignment.Center
        endDateDescriptionLabel.textColor = UIColor.darkGrayColor()
        informationContainerView.addSubview(endDateDescriptionLabel)
        
        endDateLabel.text = "End date"
        endDateLabel.font = UIFont.systemFontOfSize(16.0)
        endDateLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(endDateLabel)
        
        creationDateDescriptionLabel.text = "Creation date:"
        creationDateDescriptionLabel.font = UIFont.systemFontOfSize(14.0)
        creationDateDescriptionLabel.textAlignment = NSTextAlignment.Center
        creationDateDescriptionLabel.textColor = UIColor.darkGrayColor()
        informationContainerView.addSubview(creationDateDescriptionLabel)
        
        creationDateLabel.text = "Creation date"
        creationDateLabel.font = UIFont.systemFontOfSize(16.0)
        creationDateLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(creationDateLabel)
        
        statusLabel.text = "Status"
        statusLabel.font = UIFont.systemFontOfSize(16.0)
        statusLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(statusLabel)
        
        actionButton.setTitle("Action", forState: .Normal)
        informationContainerView.addSubview(actionButton)
        
        ownerDescriptionLabel.text = "The owner:"
        ownerDescriptionLabel.font = UIFont.systemFontOfSize(14.0)
        ownerDescriptionLabel.textAlignment = NSTextAlignment.Center
        ownerDescriptionLabel.textColor = UIColor.darkGrayColor()
        informationContainerView.addSubview(ownerDescriptionLabel)
        
        ownerNoteCosmosView.rating = 0
        ownerNoteCosmosView.settings.fillMode = .Half
        ownerNoteCosmosView.settings.starSize = 30
        ownerNoteCosmosView.settings.updateOnTouch = true
        ownerNoteCosmosView.settings.minTouchRating = 0
        ownerNoteCosmosView.didTouchCosmos = { rating in
            self.ownerNoteCosmosView.text = "[= \(rating)]"
        }
        informationContainerView.addSubview(ownerNoteCosmosView)
        
        suscriberDescriptionLabel.text = "The suscriber:"
        suscriberDescriptionLabel.font = UIFont.systemFontOfSize(14.0)
        suscriberDescriptionLabel.textAlignment = NSTextAlignment.Center
        suscriberDescriptionLabel.textColor = UIColor.darkGrayColor()
        informationContainerView.addSubview(suscriberDescriptionLabel)
        
        suscriberNoteCosmosView.rating = 0
        suscriberNoteCosmosView.settings.fillMode = .Half
        suscriberNoteCosmosView.settings.starSize = 30
        suscriberNoteCosmosView.settings.updateOnTouch = true
        suscriberNoteCosmosView.settings.minTouchRating = 0
        suscriberNoteCosmosView.didTouchCosmos =  { rating in
            self.suscriberNoteCosmosView.text = "[= \(rating)]"
        }
        informationContainerView.addSubview(suscriberNoteCosmosView)
        
        ownerButton.setTitle("Owner", forState: .Normal)
        informationContainerView.addSubview(ownerButton)
        
        ownerUsernameLabel.text = "username"
        ownerUsernameLabel.font = UIFont.systemFontOfSize(15.0)
        ownerUsernameLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(ownerUsernameLabel)
        
        suscriberButton.setTitle("Suscriber", forState: .Normal)
        informationContainerView.addSubview(suscriberButton)
        
        suscriberUsernameLabel.text = "username"
        suscriberUsernameLabel.font = UIFont.systemFontOfSize(15.0)
        suscriberUsernameLabel.textAlignment = NSTextAlignment.Center
        informationContainerView.addSubview(suscriberUsernameLabel)
    }
    
    func configureConstraints() {
        if (!didSetupConstraints) {
            scrollView.autoPinEdgesToSuperviewEdges()
            
            contentView.autoPinEdgesToSuperviewEdges()
            contentView.autoMatchDimension(.Width, toDimension: .Width, ofView: scrollView)
            
            let leadingTrailingPictureOffset: CGFloat = 0.0
            
            pictureImageView.autoMatchDimension(.Height, toDimension: .Width, ofView: contentView, withMultiplier: 0.5)
            pictureImageView.autoPinEdgeToSuperviewEdge(.Top, withInset: 0.0)
            pictureImageView.autoPinEdgeToSuperviewEdge(.Leading, withInset: leadingTrailingPictureOffset)
            pictureImageView.autoPinEdgeToSuperviewEdge(.Trailing, withInset: leadingTrailingPictureOffset)
            
            let leadingTrailingInformationContainerViewOffset: CGFloat = 16.0
            let topInformationContainerViewOffset: CGFloat = 8.0
            let leadingInformationContainerViewOffset: CGFloat = 8.0
            let trailingInformationContainerViewOffset: CGFloat = 8.0
            let bottomInformationContainerViewOffset: CGFloat = 8.0
            let bottomDownInformationContainerViewOffset: CGFloat = 15.0
            let informationVerticalSeparatorOffset: CGFloat = 9.0
            
            informationContainerView.autoPinEdge(.Top, toEdge: .Bottom, ofView: pictureImageView, withOffset: -1)
            informationContainerView.autoPinEdge(.Leading, toEdge: .Leading, ofView: pictureImageView, withOffset: leadingTrailingInformationContainerViewOffset)
            informationContainerView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: pictureImageView, withOffset: -leadingTrailingInformationContainerViewOffset)
            
            titleLabel.autoPinEdge(.Top, toEdge: .Top, ofView: informationContainerView, withOffset: topInformationContainerViewOffset * 2)
            titleLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            titleLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            categoryLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: titleLabel, withOffset: informationVerticalSeparatorOffset)
            categoryLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            categoryLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            priceLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: categoryLabel, withOffset: informationVerticalSeparatorOffset * 1.5)
            priceLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            priceLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            descriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: priceLabel, withOffset: informationVerticalSeparatorOffset * 1.5)
            descriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            descriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            mapView.autoMatchDimension(.Height, toDimension: .Width, ofView: contentView, withMultiplier: 0.5)
            mapView.autoPinEdge(.Top, toEdge: .Bottom, ofView: descriptionLabel, withOffset: informationVerticalSeparatorOffset * 1.5)
            mapView.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            mapView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            startDateDescriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: mapView, withOffset: informationVerticalSeparatorOffset * 2)
            startDateDescriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            startDateDescriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            startDateLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: startDateDescriptionLabel, withOffset: informationVerticalSeparatorOffset)
            startDateLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            startDateLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            endDateDescriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: startDateLabel, withOffset: informationVerticalSeparatorOffset)
            endDateDescriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            endDateDescriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            endDateLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: endDateDescriptionLabel, withOffset: informationVerticalSeparatorOffset)
            endDateLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            endDateLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            creationDateDescriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: endDateLabel, withOffset: informationVerticalSeparatorOffset * 2)
            creationDateDescriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            creationDateDescriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            creationDateLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: creationDateDescriptionLabel, withOffset: informationVerticalSeparatorOffset)
            creationDateLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            creationDateLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            statusLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: creationDateLabel, withOffset: informationVerticalSeparatorOffset * 2)
            statusLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            statusLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            actionButton.autoPinEdge(.Top, toEdge: .Bottom, ofView: statusLabel, withOffset: informationVerticalSeparatorOffset * 2.5)
            actionButton.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            actionButton.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            ownerDescriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: actionButton, withOffset: informationVerticalSeparatorOffset * 2.5)
            ownerDescriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            ownerDescriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            ownerNoteCosmosView.autoPinEdge(.Top, toEdge: .Bottom, ofView: ownerDescriptionLabel, withOffset: informationVerticalSeparatorOffset)
            ownerNoteCosmosView.autoAlignAxis(.Vertical, toSameAxisOfView: informationContainerView)
//            ownerNoteCosmosView.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
//            ownerNoteCosmosView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            suscriberDescriptionLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: ownerNoteCosmosView, withOffset: informationVerticalSeparatorOffset * 2)
            suscriberDescriptionLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            suscriberDescriptionLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            suscriberNoteCosmosView.autoPinEdge(.Top, toEdge: .Bottom, ofView: suscriberDescriptionLabel, withOffset: informationVerticalSeparatorOffset)
            suscriberNoteCosmosView.autoAlignAxis(.Vertical, toSameAxisOfView: informationContainerView)
//            suscriberNoteCosmosView.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
//            suscriberNoteCosmosView.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            ownerButton.autoPinEdge(.Top, toEdge: .Bottom, ofView: suscriberNoteCosmosView, withOffset: informationVerticalSeparatorOffset * 2)
            ownerButton.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            ownerButton.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            ownerUsernameLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: ownerButton, withOffset: informationVerticalSeparatorOffset)
            ownerUsernameLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            ownerUsernameLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            suscriberButton.autoPinEdge(.Top, toEdge: .Bottom, ofView: ownerUsernameLabel, withOffset: informationVerticalSeparatorOffset * 2)
            suscriberButton.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            suscriberButton.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            suscriberUsernameLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: suscriberButton, withOffset: informationVerticalSeparatorOffset)
            suscriberUsernameLabel.autoPinEdge(.Leading, toEdge: .Leading, ofView: informationContainerView, withOffset: leadingInformationContainerViewOffset)
            suscriberUsernameLabel.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: informationContainerView, withOffset: -trailingInformationContainerViewOffset)
            
            informationContainerView.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: suscriberUsernameLabel, withOffset: bottomInformationContainerViewOffset)
            informationContainerView.autoPinEdgeToSuperviewEdge(.Bottom, withInset: bottomDownInformationContainerViewOffset)
                        
            didSetupConstraints = true
        }
    }
    
    func changeServiceAnnotation(serviceAnnotation: MKPointAnnotation) {
        if (self.serviceAnnotation != nil) {
            mapView.removeAnnotation(self.serviceAnnotation!)
        }
        self.serviceAnnotation = serviceAnnotation
        
        mapView.addAnnotation(self.serviceAnnotation!)
        
        let latitudeDelta: CLLocationDegrees = 0.025
        let longitudeDelta: CLLocationDegrees = 0.025
        let span: MKCoordinateSpan = MKCoordinateSpanMake(latitudeDelta, longitudeDelta)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(self.serviceAnnotation!.coordinate, span)
        mapView.setRegion(region, animated: false)
        
        mapView.selectAnnotation(self.serviceAnnotation!, animated: true)
    }
}
