//
//  LoginView.swift
//  Heylp
//
//  Created by Alexis Lavie on 10/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    var backgroundImageView: UIImageView
    var logoContainerView: UIVisualEffectView
    var logoImageView: UIImageView
    var textFieldsContainerView: UIVisualEffectView
    var usernameTextField: UITextField
    var passwordTextField: UITextField
    var buttonsContainerView: UIVisualEffectView
    var guestContainerView: UIView
    var guestButton: UIButton
    var actionContainerView: UIView
    var actionButton: UIButton
    
    var backgroundImages: [String]
    var backgroundImageLastIndex: Int?
    var backgroundImageIsOnTheLeft: Bool
    
    var didSetupConstraints: Bool
    var lowestBottomConstraintUnderKeyboard: NSLayoutConstraint?
    var lowestBottomConstraintWithKeyboardShown: NSLayoutConstraint?
    
    init(_ coder: NSCoder? = nil, _ frame: CGRect? = nil) {
        self.backgroundImageView = UIImageView()
        self.logoContainerView = UIVisualEffectView()
        self.logoImageView = UIImageView()
        self.textFieldsContainerView = UIVisualEffectView()
        self.usernameTextField = UITextField()
        self.passwordTextField = UITextField()
        self.buttonsContainerView = UIVisualEffectView()
        self.guestContainerView = UIView()
        self.guestButton = UIButton(type: UIButtonType.System)
        self.actionContainerView = UIView()
        self.actionButton = UIButton(type: UIButtonType.System)
        
        self.backgroundImages = ["backgrounds/babysitting.jpg", "backgrounds/beauty.jpg", "backgrounds/computer.jpg", "backgrounds/delivery.jpg", "backgrounds/do_it_yourself-1.jpg", "backgrounds/do_it_yourself-2.jpg", "backgrounds/events.jpg", "backgrounds/gardening.jpg", "backgrounds/homework_assistance.jpg", "backgrounds/housekeeping.jpg", "backgrounds/other.jpg", "backgrounds/pets.jpg"]
        self.backgroundImageIsOnTheLeft = false
        
        self.didSetupConstraints = false
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(frame: frame!)
        }
    }

    required convenience init(coder: NSCoder) {
        self.init(coder, nil)
    }
    
    override required convenience init(frame: CGRect) {
        self.init(nil, frame)
    }
    
    func configure() {
        backgroundImageView.frame = CGRectMake(0.0, 0.0, 0.0, frame.size.height)
        addSubview(backgroundImageView)
        
        logoContainerView.effect = UIBlurEffect(style: .ExtraLight)
        logoContainerView.layer.cornerRadius = 25.0
        logoContainerView.clipsToBounds = true
        addSubview(logoContainerView)
        
        logoImageView.image = UIImage(named: "heylp-60@3x.png")
        logoImageView.contentMode = UIViewContentMode.ScaleAspectFit
        logoContainerView.addSubview(logoImageView)
        
        textFieldsContainerView.effect = UIBlurEffect(style: .Light)
        addSubview(textFieldsContainerView)
        
//        usernameTextField.placeholder = "Username"
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Username", attributes:  [NSForegroundColorAttributeName: UIColor.lightTextColor()])
        usernameTextField.borderStyle = UITextBorderStyle.None
        usernameTextField.textAlignment = NSTextAlignment.Center
        usernameTextField.font = UIFont.systemFontOfSize(15.0)
        usernameTextField.textColor = UIColor.whiteColor()
        usernameTextField.keyboardType = UIKeyboardType.ASCIICapable
        usernameTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        usernameTextField.autocapitalizationType = UITextAutocapitalizationType.None;
        usernameTextField.autocorrectionType = UITextAutocorrectionType.No
        usernameTextField.returnKeyType = UIReturnKeyType.Next
        usernameTextField.nextField = passwordTextField
        textFieldsContainerView.addSubview(usernameTextField)
        
        passwordTextField.secureTextEntry = true
//        passwordTextField.placeholder = "Password"
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes:  [NSForegroundColorAttributeName: UIColor.lightTextColor()])
        passwordTextField.borderStyle = UITextBorderStyle.None
        passwordTextField.textAlignment = NSTextAlignment.Center
        passwordTextField.font = UIFont.systemFontOfSize(15.0)
        passwordTextField.textColor = UIColor.whiteColor()
        passwordTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordTextField.returnKeyType = UIReturnKeyType.Done
        textFieldsContainerView.addSubview(passwordTextField)
        
        buttonsContainerView.effect = UIBlurEffect(style: .Light)
        addSubview(buttonsContainerView)
        
        buttonsContainerView.addSubview(guestContainerView)
        
        guestButton.setTitle("Guest", forState: .Normal)
        guestButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        guestContainerView.addSubview(guestButton)
        
        buttonsContainerView.addSubview(actionContainerView)
        
        actionButton.setTitle("Sign Up", forState: .Normal)
        actionButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        actionContainerView.addSubview(actionButton)
    }
    
    func configureConstraints() {
        if (!didSetupConstraints) {
            logoContainerView.autoMatchDimension(.Width, toDimension: .Width, ofView: self, withMultiplier: 0.3)
            logoContainerView.autoMatchDimension(.Height, toDimension: .Width, ofView: logoContainerView)
            logoContainerView.autoAlignAxisToSuperviewAxis(.Vertical)
            logoContainerView.autoPinToTopLayoutGuideOfViewController(parentViewController!, withInset: 50.0)
            
            logoImageView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(8.0, 8.0, 8.0, 8.0))
            
            textFieldsContainerView.autoAlignAxisToSuperviewAxis(.Vertical)
            textFieldsContainerView.autoMatchDimension(.Width, toDimension: .Width, ofView: self)
            lowestBottomConstraintUnderKeyboard = textFieldsContainerView.autoPinEdge(.Bottom, toEdge: .Top, ofView: self.buttonsContainerView, withOffset: -58.0)
            NSLayoutConstraint.autoCreateConstraintsWithoutInstalling({
                self.lowestBottomConstraintWithKeyboardShown = self.textFieldsContainerView.autoPinToBottomLayoutGuideOfViewController(self.parentViewController!, withInset: 0.0)
            })
            
            usernameTextField.autoAlignAxisToSuperviewAxis(.Vertical)
            usernameTextField.autoPinEdge(.Leading, toEdge: .Leading, ofView: textFieldsContainerView, withOffset: 8.0)
            usernameTextField.autoPinEdge(.Trailing, toEdge: .Trailing, ofView: textFieldsContainerView, withOffset: -8.0)
            usernameTextField.autoPinEdge(.Top, toEdge: .Top, ofView: textFieldsContainerView, withOffset: 8.0)
            
            passwordTextField.autoAlignAxisToSuperviewAxis(.Vertical)
            passwordTextField.autoMatchDimension(.Width, toDimension: .Width, ofView: usernameTextField)
            passwordTextField.autoPinEdge(.Top, toEdge: .Bottom, ofView: usernameTextField, withOffset: 12.0)
            passwordTextField.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: textFieldsContainerView, withOffset: -8.0)
            
            buttonsContainerView.autoAlignAxisToSuperviewAxis(.Vertical)
            buttonsContainerView.autoConstrainAttribute(.Leading, toAttribute: .Leading, ofView: self)
            buttonsContainerView.autoConstrainAttribute(.Trailing, toAttribute: .Trailing, ofView: self)
            buttonsContainerView.autoPinToBottomLayoutGuideOfViewController(parentViewController!, withInset: 0.0)

            guestContainerView.autoMatchDimension(.Width, toDimension: .Width, ofView: buttonsContainerView, withMultiplier: 0.5)
            guestContainerView.autoPinEdgeToSuperviewEdge(.Leading)
            guestContainerView.autoPinEdgeToSuperviewEdge(.Top)
            guestContainerView.autoPinEdgeToSuperviewEdge(.Bottom)
            
            guestButton.autoAlignAxisToSuperviewAxis(.Vertical)
            guestButton.autoPinEdgeToSuperviewEdge(.Top)
            guestButton.autoPinEdgeToSuperviewEdge(.Bottom)
            
            actionContainerView.autoMatchDimension(.Width, toDimension: .Width, ofView: buttonsContainerView, withMultiplier: 0.5)
            actionContainerView.autoPinEdgeToSuperviewEdge(.Trailing)
            actionContainerView.autoPinEdgeToSuperviewEdge(.Top)
            actionContainerView.autoPinEdgeToSuperviewEdge(.Bottom)
            
            actionButton.autoAlignAxisToSuperviewAxis(.Vertical)
            actionButton.autoPinEdgeToSuperviewEdge(.Top)
            actionButton.autoPinEdgeToSuperviewEdge(.Bottom)
            
            didSetupConstraints = true
        }
    }
    
    func backgroundImageViewWidth(backgroundImage: UIImage) -> CGFloat {
        let backgroundImageViewScale = frame.size.height / backgroundImage.size.height
        
        return backgroundImage.size.width * backgroundImageViewScale
    }
    
    func changeActiveBackgroundImage(animated animated: Bool) {
        var index: Int
        
        repeat {
            index = Int(arc4random_uniform(UInt32(backgroundImages.count)))
        } while (index == backgroundImageLastIndex)
        
        backgroundImageLastIndex = index
        
        if (animated) {
            UIView.transitionWithView(backgroundImageView, duration: 1.0,
                options: .TransitionCrossDissolve,
                animations: {
//                    self.backgroundImageView.image = UIImage(named: self.backgroundImages[index])
                    self.backgroundImageView.image = UIImage.getUncachedImage(named: self.backgroundImages[index])
                }, completion: nil)
        } else {
//            backgroundImageView.image = UIImage(named: backgroundImages[index])
            self.backgroundImageView.image = UIImage.getUncachedImage(named: self.backgroundImages[index])
        }
        
        backgroundImageView.frame.size.width = backgroundImageViewWidth(backgroundImageView.image!)
        
        if (backgroundImageIsOnTheLeft) {
            backgroundImageView.frame.origin.x = -backgroundImageViewWidth(backgroundImageView.image!) + frame.size.width
        } else {
            backgroundImageView.frame.origin.x = 0.0
        }
    }

    func runBackgroundAnimation() {
        backgroundImageIsOnTheLeft = !backgroundImageIsOnTheLeft
        
        if (backgroundImageIsOnTheLeft) {
            UIView.animateAndChainWithDuration(15.0, delay: 0.0,
                options: [.CurveLinear],
                animations: {
                    self.backgroundImageView.frame.origin.x = -self.backgroundImageViewWidth(self.backgroundImageView.image!) + self.frame.size.width
                }, completion: { finished -> Void in
                    if (finished) {
                        self.changeActiveBackgroundImage(animated: true)
                        self.runBackgroundAnimation()
                    }
            })
        } else {
            UIView.animateAndChainWithDuration(15.0, delay: 0.0,
                options: [.CurveLinear], animations: {
                    self.backgroundImageView.frame.origin.x = 0.0
                }, completion: { finished -> Void in
                    if (finished) {
                        self.changeActiveBackgroundImage(animated: true)
                        self.runBackgroundAnimation()
                    }
            })
        }
    }
    
    func stopBackgroundAnimation() {
        backgroundImageView.layer.removeAllAnimations()
//        backgroundImageView.image = nil
    }

    func adjustlowestBottomConstraint(isKeyboardShown: Bool, notification: NSNotification) {
        let userInfo = notification.userInfo!
        let keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        let changeInHeight = CGRectGetHeight(keyboardFrame) + 8.0
        
        UIView.animateWithDuration(animationDuration, animations: { () -> Void in
            if (isKeyboardShown) {
                self.lowestBottomConstraintUnderKeyboard?.active = false
                self.lowestBottomConstraintWithKeyboardShown?.constant = -changeInHeight
                self.lowestBottomConstraintWithKeyboardShown?.active = true
                self.logoContainerView.effect = UIBlurEffect(style: .Dark)
                self.textFieldsContainerView.effect = UIBlurEffect(style: .Dark)
            } else {
                self.lowestBottomConstraintWithKeyboardShown?.active = false
                self.lowestBottomConstraintUnderKeyboard?.active = true
                self.logoContainerView.effect = UIBlurEffect(style: .ExtraLight)
                self.textFieldsContainerView.effect = UIBlurEffect(style: .Light)
            }
        })
    }
}
