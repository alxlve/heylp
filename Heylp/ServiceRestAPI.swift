//
//  ServiceRestAPI.swift
//  Heylp
//
//  Created by Alexis Lavie on 15/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import Alamofire
import ObjectMapper

class ServiceRestAPI: RestAPI<Service> {

    internal var userContextUrl: String
    internal var userId: UInt {
        didSet {
            self.fullUrl = self.userContextUrl + "/" + String(self.userId) + "/" + self.contextUrl
        }
    }
    
    init(baseUrl: String, userContextUrl: String, contextUrl: String) {
        self.userContextUrl = userContextUrl
        self.userId = 0
        
        super.init()
        
        self.baseUrl = baseUrl
        self.contextUrl = contextUrl
    }
    
    func getUserAllAffectedServices(completion completion: (responseObject: [Service]?, error: NSError?) -> ()) {
        Alamofire.request(.GET, baseUrl + "/" + userContextUrl + "/" + String(userId) + "/service-affected").responseArray { (response: Response<[Service], NSError>) in
            let services = response.result.value
            if services == nil {
                completion(responseObject: [Service](), error: response.result.error)
            } else {
                completion(responseObject: services, error: response.result.error)
            }
        }
    }
    
    func getUserAllAccomplishedServices(completion completion: (responseObject: [Service]?, error: NSError?) -> ()) {
        Alamofire.request(.GET, baseUrl + "/" + userContextUrl + "/" + String(userId) + "/service-accomplished").responseArray { (response: Response<[Service], NSError>) in
            let services = response.result.value
            if services == nil {
                completion(responseObject: [Service](), error: response.result.error)
            } else {
                completion(responseObject: services, error: response.result.error)
            }
        }
    }
    
    func getServicePicture(serviceId: UInt, completion: (responseObject: Picture?) -> ()) {
        Alamofire.request(.GET, fullUrl + "/\(serviceId)" + "/picture").responseObject { (response: Response<Picture, NSError>) in
            let object = response.result.value
            completion(responseObject: object)
        }
    }
    
    func addServicePicture(serviceId: UInt, object: Picture, completionHandler: (responseObject: UInt?) -> ()) {
        //        let request = NSMutableURLRequest(URL: NSURL(string: fullUrl)!)
        //        request.HTTPMethod = "POST"
        //        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameters = Mapper().toJSON(object)
        
        Alamofire.request(.POST, fullUrl + "/\(serviceId)" + "/picture", parameters: parameters, encoding: .JSON)
            .responseString { response in
                switch response.result {
                case .Success(let value):
                    completionHandler(responseObject: UInt(value))
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
    }
    
    func getAllAvailableServices(completionHandler: (responseObject: [Service]?, error: NSError?) -> ()) {
        Alamofire.request(.GET, baseUrl + "/_service/available").responseArray { (response: Response<[Service], NSError>) in
            let services = response.result.value
            if services == nil {
                completionHandler(responseObject: [Service](), error: response.result.error)
            } else {
                completionHandler(responseObject: services, error: response.result.error)
            }
        }
    }
    
    func getAllAvailableServices(serviceCategory: ServiceCategory, completionHandler: (responseObject: [Service]?, error: NSError?) -> ()) {
        let parameters: [String: AnyObject] = [
            "category": serviceCategory.rawValue
        ]
        
        Alamofire.request(.POST, baseUrl + "/_service/available/", parameters: parameters, encoding: .JSON).responseArray { (response: Response<[Service], NSError>) in
            let services = response.result.value
            if services == nil {
                completionHandler(responseObject: [Service](), error: response.result.error)
            } else {
                completionHandler(responseObject: services, error: response.result.error)
            }
        }
    }
}
