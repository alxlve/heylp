//
//  ProfileLabelCell.swift
//  Former-Demo
//
//  Created by Alexis Lavie on 12/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import Former

final class ProfileLabelCell: UITableViewCell, InlineDatePickerFormableRow, InlinePickerFormableRow {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var displayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = .formerColor()
        displayLabel.textColor = .formerSubColor()
    }
    
    func formTitleLabel() -> UILabel? {
        return titleLabel
    }
    
    func formDisplayLabel() -> UILabel? {
        return displayLabel
    }
    
    func updateWithRowFormer(rowFormer: RowFormer) {}
}