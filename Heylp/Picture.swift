//
//  Picture.swift
//  Heylp
//
//  Created by Alexis Lavie on 27/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

class Picture: Mappable {
    
    var id: UInt?
    var image: UIImage!
    
    init(image: UIImage) {
        self.id = nil
        self.image = image
    }
    
    init() {}
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id             <- (map["id"], NoTransform(serialization: true))
        self.image        <- (map["picture"], UIImageTransform())
    }
}