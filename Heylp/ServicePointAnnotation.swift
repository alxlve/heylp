//
//  ServicePointAnnotation.swift
//  Heylp
//
//  Created by Alexis Lavie on 22/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit

class ServicePointAnnotation: ColorPointAnnotation {
    
    var service: Service!
    
    init(service: Service) {
        self.service = service
        
        super.init(pinColor: UIColor(red: 255/255, green: 59/255, blue: 48/255, alpha: 1.0))
    }
}