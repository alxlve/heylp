//
//  BrowseCategoryView.swift
//  Heylp
//
//  Created by Alexis Lavie on 17/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

class BrowseCategoryView: UIView {
    
    var tableView: UITableView
    
    var didSetupConstraints: Bool
    
    init(_ coder: NSCoder? = nil, _ frame: CGRect? = nil) {
        self.tableView = UITableView()

        self.didSetupConstraints = false
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(frame: frame!)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder, nil)
    }
    
    override required convenience init(frame: CGRect) {
        self.init(nil, frame)
    }
    
    func configure() {
        backgroundColor = UIColor(patternImage: UIImage(named: "grid.png")!)
        
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor(red: 0, green: 0.478431, blue: 1.0, alpha: 0.5)
        tableView.separatorInset = UIEdgeInsetsMake(0.0, 15.0, 0.0, 15.0)
        tableView.layoutMargins = UIEdgeInsetsZero
        addSubview(tableView)
    }
    
    func configureConstraints() {
        if (!didSetupConstraints) {
            tableView.autoPinToTopLayoutGuideOfViewController(parentViewController!, withInset: 0.0)
            tableView.autoPinToBottomLayoutGuideOfViewController(parentViewController!, withInset: 0.0)
            tableView.autoPinEdgeToSuperviewEdge(.Leading)
            tableView.autoPinEdgeToSuperviewEdge(.Trailing)
            
            didSetupConstraints = true
        }
    }
}
