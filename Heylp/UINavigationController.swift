//
//  UINavigationController.swift
//  Heylp
//
//  Created by Alexis Lavie on 23/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    public override func shouldAutorotate() -> Bool {
        if (topViewController != nil) {
            return topViewController!.shouldAutorotate()
        }
        
        return true
    }
    
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if (topViewController != nil) {
            return topViewController!.supportedInterfaceOrientations()
        }
        
        return UIInterfaceOrientationMask.All
    }
    
    public override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        if (topViewController != nil) {
            return topViewController!.preferredInterfaceOrientationForPresentation()
        }
        
        return UIInterfaceOrientation.Portrait
    }
}
