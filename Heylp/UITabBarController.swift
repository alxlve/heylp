//
//  UITabBarController.swift
//  Heylp
//
//  Created by Alexis Lavie on 25/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

extension UITabBarController {
    
    public override func shouldAutorotate() -> Bool {
        if (selectedViewController != nil) {
            return selectedViewController!.shouldAutorotate()
        }
        
        return true
    }
    
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if (selectedViewController != nil) {
            return selectedViewController!.supportedInterfaceOrientations()
        }
        
        return UIInterfaceOrientationMask.All
    }
    
    public override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        if (selectedViewController != nil) {
            return selectedViewController!.preferredInterfaceOrientationForPresentation()
        }
        
        return UIInterfaceOrientation.Portrait
    }
}
