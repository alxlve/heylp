//
//  Phone.swift
//  Heylp
//
//  Created by Alexis Lavie on 04/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

enum PhoneType: String, CustomStringConvertible {
    case Home
    case Work
    case Fax
    case Mobile
    
    var description: String {
        switch self {
            case .Home: return "Home"
            case .Work: return "Work"
            case .Fax: return "Fax"
            case .Mobile: return "Mobile"
        }
    }
    
    static func values() -> [PhoneType] {
        return [.Home, .Work, .Fax, .Mobile]
    }
}

class Phone: Mappable {

    var id: UInt?
    var owner: UInt?
    var countryCode: Int!
    var number: Int!
    var phoneType: PhoneType!

    init(countryCode: Int, number: Int, phoneType: PhoneType) {
        self.id = nil
        self.countryCode = countryCode
        self.number = number
        self.phoneType = phoneType
    }
    
    init() {}
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id         <- (map["id"], NoTransform(serialization: false))
        self.owner          <- (map["owner"], NoTransform(serialization: false))
        self.countryCode    <- map["countryCode"]
        self.number         <- map["number"]
        self.phoneType      <- (map["phoneType"], EnumTransform())
    }
}
