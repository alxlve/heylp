//
//  NoTransform.swift
//  Heylp
//
//  Created by Alexis Lavie on 05/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

class NoTransform<T>: TransformType {
    typealias Object = T
    typealias JSON = T
    
    var deserialization: Bool
    var serialization: Bool
    
    init(deserialization: Bool, serialization: Bool) {
        self.deserialization = deserialization
        self.serialization = serialization
    }
    
    convenience init() {
        self.init(deserialization: true, serialization: true)
    }
    
    convenience init(serialization: Bool) {
        self.init(deserialization: true, serialization: serialization)
    }
    
    func transformFromJSON(value: AnyObject?) -> T? {
        if (deserialization) {
            if let value = value as? T {
                return value
            }
        }
        
        return nil
    }
    
    func transformToJSON(value: T?) -> T? {
        if (serialization) {
            return value
        }
        
        return nil
    }
}
