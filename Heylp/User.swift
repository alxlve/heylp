//
//  User.swift
//  Heylp
//
//  Created by Alexis Lavie on 04/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

enum Gender: String, CustomStringConvertible {
    case Male
    case Female
    
    var description: String {
        switch self {
            case .Male: return "Male"
            case .Female: return "Female"
        }
    }
    
    static func values() -> [Gender] {
        return [.Male, .Female]
    }
}

class User: Mappable {

    var id: UInt?
    var username: String!
    var password: String!
    var email: String!
    var picture: Picture?
    var firstName: String!
    var lastName: String!
    var gender: Gender!
    var birthday: NSDate!
    var ownerNote: [Float]?
    var suscriberNote: [Float]?
    var address: Address!
    var phones: [Phone]!
    var acceptedServices: [Service]!
    var proposedServices: [Service]!
    
    init(username: String, password: String, email: String, firstName: String, lastName: String, gender: Gender, birthday: NSDate, address: Address, phones: [Phone]?, acceptedServices: [Service]?, proposedServices: [Service]?) {
        self.id = nil
        self.username = username
        self.password = password
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.gender = gender
        self.birthday = birthday
        self.address = address
        if let _ = phones {
            self.phones = phones!
        } else {
            self.phones = [Phone]()
        }
        if let _ = acceptedServices {
            self.acceptedServices = acceptedServices!
        } else {
            self.acceptedServices = [Service]()
        }
        if let _ = proposedServices {
            self.proposedServices = proposedServices!
        } else {
            self.proposedServices = [Service]()
        }
    }
    
    init() {}
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id             <- (map["id"], NoTransform(serialization: false))
        self.username       <- map["username"]
        self.password       <- map["password"]
        self.email          <- map["email"]
//        self.picture        <- map["picture"]
        self.firstName      <- map["firstName"]
        self.lastName       <- map["lastName"]
        self.gender         <- (map["gender"], EnumTransform())
        self.birthday       <- (map["birthday"], JavaDateTransform())
        self.ownerNote      <- (map["ownerNote"], ArrayNoTransform(serialization: false))
        self.suscriberNote  <- (map["suscriberNote"], ArrayNoTransform(serialization: false))
        self.address        <- map["address"]
    }
}
