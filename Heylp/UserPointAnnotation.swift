//
//  UserPointAnnotation.swift
//  Heylp
//
//  Created by Alexis Lavie on 22/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit

class UserPointAnnotation: ColorPointAnnotation {
    
    var user: User?
    
    init(user: User?) {
        self.user = user
        
        super.init(pinColor: UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0))
    }
}
