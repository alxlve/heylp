//
//  BrowseCategoryViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 17/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

class BrowseCategoryViewController: UIViewController {
    
    var serviceCategories: [ServiceCategory]
    
    var myView: BrowseCategoryView! {
        return self.view as! BrowseCategoryView
    }
    
    init(_ coder: NSCoder? = nil) {
        serviceCategories = ServiceCategory.values()
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    func add() {
        let addServiceViewController = AddServiceViewController()
        self.navigationController?.pushViewController(addServiceViewController, animated: true)
    }
    
    private func configure() {
        title = "Services"
        
        if (App.sharedInstance.getConnectedUser() != nil) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "add")
        }
    }
    
    override func loadView() {
        super.loadView()
        view = BrowseCategoryView(frame: UIScreen.mainScreen().bounds)
        
        configure()
        myView.configure()
        myView.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        myView.configureConstraints()
        
        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        
        // Delegates
        myView.tableView.delegate = self
        myView.tableView.dataSource = self
                
//        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
    }
}


// MARK - UITableViewDelegate, UITableViewDataSource
extension BrowseCategoryViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + serviceCategories.count
    }
    
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//            var cell = tableView.dequeueReusableCellWithIdentifier("UITableViewCell", forIndexPath: indexPath)
            var cell = tableView.dequeueReusableCellWithIdentifier("UITableViewCell")
            
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "UITableViewCell")
            }
            
            if (indexPath.row == 0) {
                cell!.textLabel?.text = "All"
                cell!.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 0.75)
            } else {
                let serviceCategory = serviceCategories[indexPath.row - 1]
            
                cell!.textLabel?.text = serviceCategory.description
//                cell!.detailTextLabel?.text = "Details"
                cell!.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.60)
            }
            
            cell!.selectionStyle = UITableViewCellSelectionStyle.Default
            let customColorView = UIView()
            customColorView.backgroundColor = UIColor(red: 0, green: 0.478431, blue: 1.0, alpha: 0.5)
            cell!.selectedBackgroundView = customColorView
            
            cell!.textLabel?.textColor = UIColor.blackColor()
            
            return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let servicesViewController = ServicesViewController()
        servicesViewController.mode = "category"
        
        if (indexPath.row == 0) {
            servicesViewController.serviceCategory = nil
            self.navigationController?.pushViewController(servicesViewController, animated: true)
        } else {
            servicesViewController.serviceCategory = self.serviceCategories[indexPath.row - 1]
            self.navigationController?.pushViewController(servicesViewController, animated: true)
        }
        
        myView.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
