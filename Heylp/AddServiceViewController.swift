//
//  AddServiceViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 18/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

class AddServiceViewController: UIViewController {
    
    var myView: AddServiceView! {
        return self.view as! AddServiceView
    }
    
    var category: ServiceCategory?
    var title0: String?
    var description0: String?
    var picture: UIImage?
    var price: Float?
    var startDate: NSDate?
    var endDate: NSDate?
    var creationDate: NSDate?
    var streetNumber: String?
    var streetName: String?
    var city: String?
    var postalCode: String?
    var country: String?
    
    init(_ coder: NSCoder? = nil) {
        if let coder = coder {
            super.init(coder: coder)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    func save() {
        if (category != nil && title0 != nil && description0 != nil && picture != nil && price != nil && startDate != nil && endDate != nil && creationDate != nil && streetNumber != nil && streetName != nil && city != nil && postalCode != nil && country != nil) {
            
            self.navigationItem.rightBarButtonItem = nil
            let address = Address(streetNumber: streetNumber!, streetName: streetName!, city: city!, postalCode: postalCode!, country: country!)
            address.updateCoordinates(completion: {
                let service = Service(category: self.category!, title: self.title0!, description: self.description0!, address: address, price: self.price!, startDate: self.startDate!, endDate: self.endDate!, creationDate: self.creationDate!, status: ServiceStatus.Available)
                
                ModelAPI.sharedInstance.addUserProposedService(App.sharedInstance.getConnectedUser()!, service: service, completionHandler: {
                    ModelAPI.sharedInstance.addServicePicture(App.sharedInstance.getConnectedUser()!.id!, service: service, picture: Picture(image: self.picture!), completion: {
                        self.navigationController?.popViewControllerAnimated(true)
                    })
                })
            })
        }
    }
    
    private func configure() {
        title = "Service"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: "save")
    }
    
    internal func presentImagePicker() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .PhotoLibrary
        picker.allowsEditing = false
        picker.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        presentViewController(picker, animated: true, completion: nil)
//        addChildViewController(picker)
//        picker.didMoveToParentViewController(self)
//        self.myView.addSubview(picker.view)
    }
    
    override func loadView() {
        super.loadView()
        view = AddServiceView(frame: UIScreen.mainScreen().bounds)
        
        configure()
        myView.configure()
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        myView.configureConstraints()
        
        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
    }
}

// MARK: - UITextFieldDelegate
extension AddServiceViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension AddServiceViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismissViewControllerAnimated(true, completion: nil)
//        picker.view.removeFromSuperview()
//        picker.removeFromParentViewController()
        self.picture = image
        myView.pictureRow.cellUpdate {
            $0.iconView.image = image
        }
    }
}
