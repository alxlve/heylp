//
//  HomeTabBarController.swift
//  Heylp
//
//  Created by Alexis Lavie on 17/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        let mapViewController = MapViewController()
        let mapViewControllerItem = UITabBarItem(title: "Map", image: UIImage(named: "near"), selectedImage: UIImage(named: "near"))
        mapViewController.tabBarItem = mapViewControllerItem
        let mapNavigationController = UINavigationController()
        mapNavigationController.viewControllers = [mapViewController]
        
        let browseCategoryViewController = BrowseCategoryViewController()
        let browseCategoryViewControllerItem = UITabBarItem(title: "Services", image: UIImage(named: "search"), selectedImage: UIImage(named: "search"))
        browseCategoryViewController.tabBarItem = browseCategoryViewControllerItem
        let browseNavigationController = UINavigationController()
        browseNavigationController.viewControllers = [browseCategoryViewController]
        
        if (App.sharedInstance.getConnectedUser() == nil) {
            let controllers = [mapNavigationController, browseNavigationController]
            viewControllers = controllers
        } else {
            let profileViewController = UserDetailViewController()
            profileViewController.mode = "self"
            profileViewController.userId = App.sharedInstance.getConnectedUser()!.id
            let homeViewControllerItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
            profileViewController.tabBarItem = homeViewControllerItem
            let profileNavigationController = UINavigationController()
            profileNavigationController.viewControllers = [profileViewController]
            
            let controllers = [profileNavigationController, mapNavigationController, browseNavigationController]
            viewControllers = controllers
        }
    }
}

extension HomeTabBarController: UITabBarControllerDelegate {

    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        if let viewController = viewController as? UINavigationController {
            viewController.popToRootViewControllerAnimated(false)
        }
        
        return true
    }
}