//
//  MapView.swift
//  Heylp
//
//  Created by Alexis Lavie on 21/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit

class MapView: UIView {
    
    var mapView: MKMapView
    var currentAnnotation: UserPointAnnotation
    var serviceAnnotations: [ServicePointAnnotation]
    var userAnnotations: [UserPointAnnotation]
    
    var didSetupConstraints: Bool
    
    init(_ coder: NSCoder? = nil, _ frame: CGRect? = nil) {
        self.mapView = MKMapView()
        self.currentAnnotation = UserPointAnnotation(user: App.sharedInstance.getConnectedUser())
        self.serviceAnnotations = [ServicePointAnnotation]()
        self.userAnnotations = [UserPointAnnotation]()
    
        self.didSetupConstraints = false
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(frame: frame!)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder, nil)
    }
    
    override required convenience init(frame: CGRect) {
        self.init(nil, frame)
    }
    
    func configure() {
        backgroundColor = UIColor.whiteColor()
        
        addSubview(mapView)
        
        currentAnnotation.pinColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1.0)
        currentAnnotation.title = "Me"
        currentAnnotation.subtitle = "I'm actually here!"
        
        mapView.addAnnotation(currentAnnotation)
        mapView.selectAnnotation(currentAnnotation, animated: true)
    }
    
    func configureConstraints() {
        if (!didSetupConstraints) {
            mapView.autoPinEdgesToSuperviewEdges();
//            mapView.autoPinToTopLayoutGuideOfViewController(self.parentViewController!, withInset: 0.0)
//            mapView.autoPinToBottomLayoutGuideOfViewController(self.parentViewController!, withInset: 0.0)
//            mapView.autoPinEdgeToSuperviewEdge(.Leading)
//            mapView.autoPinEdgeToSuperviewEdge(.Trailing)
            
            didSetupConstraints = true
        }
    }
    
    func addServiceAnnotation(serviceAnnotation: ServicePointAnnotation) {
        serviceAnnotations.append(serviceAnnotation)
        mapView.addAnnotation(serviceAnnotation)
    }
    
    func removeServiceAnnotations() {
        for serviceAnnotation in serviceAnnotations {
            mapView.removeAnnotation(serviceAnnotation)
        }
        
        serviceAnnotations = [ServicePointAnnotation]()
    }
    
    func addUserAnnotation(userAnnotation: UserPointAnnotation) {
        userAnnotations.append(userAnnotation)
        mapView.addAnnotation(userAnnotation)
    }
    
    func removeUserAnnotations() {
        for userAnnotation in userAnnotations {
            mapView.removeAnnotation(userAnnotation)
        }
        
        userAnnotations = [UserPointAnnotation]()
    }
}
