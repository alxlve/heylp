//
//  RestAPI.swift
//  Heylp
//
//  Created by Alexis Lavie on 06/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON

class RestAPI<T: Mappable>: ModelAPIProtocol {
    
    internal var baseUrl: String!
    internal var contextUrl: String!
    internal var fullUrl: String! {
        didSet {
            self.fullUrl = baseUrl + "/" + self.fullUrl
        }
    }
    
    func get(id: UInt, completionHandler: (responseObject: T?) -> ()) {
        Alamofire.request(.GET, fullUrl + "/\(id)").responseObject { (response: Response<T, NSError>) in
            let object = response.result.value
            completionHandler(responseObject: object)
        }
    }
    
    func getAll(completionHandler completionHandler: (responseObject: [T]) -> ()) {
        Alamofire.request(.GET, fullUrl).responseArray { (response: Response<[T], NSError>) in
            let objects = response.result.value
            if objects == nil {
                completionHandler(responseObject: [T]())
            } else {
                completionHandler(responseObject: objects!)
            }
        }
    }
    
    func add(object: T, completionHandler: (responseObject: UInt?) -> ()) {
//        let request = NSMutableURLRequest(URL: NSURL(string: fullUrl)!)
//        request.HTTPMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let parameters = Mapper().toJSON(object)
        
        Alamofire.request(.POST, fullUrl, parameters: parameters, encoding: .JSON)
            .responseString { response in
                switch response.result {
                    case .Success(let value):
                        completionHandler(responseObject: UInt(value))
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                }
            }
    }
    
    func save(id: UInt, object: T, completion: () -> ()) {
        let parameters = Mapper().toJSON(object)
        
        Alamofire.request(.PUT, fullUrl + "/\(id)", parameters: parameters, encoding: .JSON)
            .responseString { response in
                switch response.result {
                case .Success:
                    completion()
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
    }
    
    func delete(item: T) {
        
    }
}
