//
//  ProfileFieldCell.swift
//  Former-Demo
//
//  Created by Alexis Lavie on 12/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import Former

final class ProfileFieldCell: UITableViewCell, TextFieldFormableRow {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = .formerColor()
        textField.textColor = .formerSubColor()
    }
    
    func formTextField() -> UITextField {
        return textField
    }
    
    func formTitleLabel() -> UILabel? {
        return titleLabel
    }
    
    func updateWithRowFormer(rowFormer: RowFormer) {}
}