//
//  AddServiceView.swift
//  Heylp
//
//  Created by Alexis Lavie on 18/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import Former

class AddServiceView: UIView {
    
    var parentVC: AddServiceViewController!
    var tableView: UITableView
    
    var didSetupConstraints = false
    
    var former: Former
    
    init(_ coder: NSCoder? = nil, _ frame: CGRect? = nil) {
        self.tableView = UITableView(frame: CGRect.zero, style: .Grouped)
        self.former = Former(tableView: tableView)
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(frame: frame!)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder, nil)
    }
    
    override required convenience init(frame: CGRect) {
        self.init(nil, frame)
    }
    
    lazy var pictureRow: LabelRowFormer<ProfileImageCell> = {
        LabelRowFormer<ProfileImageCell>(instantiateType: .Nib(nibName: "ProfileImageCell")) {
            $0.iconView.image = nil
            }.configure {
                $0.text = "Choose image from library"
                $0.rowHeight = 60
            }.onSelected { [weak self] _ in
                self?.former.deselect(true)
                self!.parentVC.presentImagePicker()
        }
    }()
    
    var startDateRow: InlineDatePickerRowFormer<ProfileLabelCell>!
    var startDatePicker: UIDatePicker!
    var endDateRow: InlineDatePickerRowFormer<ProfileLabelCell>!
    var endDatePicker: UIDatePicker!
    
    func configure() {
        parentVC = parentViewController as! AddServiceViewController
        
        backgroundColor = .groupTableViewBackgroundColor()
        
        tableView.backgroundColor = .clearColor()
        insertSubview(tableView, atIndex: 0)
        
        // Create RowFormers
        let categoryRow = InlinePickerRowFormer<ProfileLabelCell, ServiceCategory>(instantiateType: .Nib(nibName: "ProfileLabelCell")) {
            $0.titleLabel.text = "Category"
            }.configure {
                self.parentVC.category = ServiceCategory.values()[0]
                $0.pickerItems = ServiceCategory.values().map {
                    InlinePickerItem(title: $0.description, value: $0)
                }
            }.onValueChanged {
                self.parentVC.category = $0.value
        }
        let titleRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Title"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter the title"
            }.onTextChanged {
                self.parentVC.title0 = $0
        }
        let descriptionRow = TextViewRowFormer<FormTextViewCell>() {
            $0.textView.textColor = .formerSubColor()
            $0.textView.font = .systemFontOfSize(15)
            $0.textView.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter the description"
            }.onTextChanged {
                self.parentVC.description0 = $0
        }
        let streetNumberRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Street number"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.keyboardType = UIKeyboardType.NumbersAndPunctuation
            }.configure {
                $0.placeholder = "Enter the street number"
            }.onTextChanged {
                self.parentVC.streetNumber = $0
        }
        let streetNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Street name"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.autocapitalizationType = UITextAutocapitalizationType.None;
            }.configure {
                $0.placeholder = "Enter the street name"
            }.onTextChanged {
                self.parentVC.streetName = $0
        }
        let cityRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "City"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter the city"
            }.onTextChanged {
                self.parentVC.city = $0
        }
        let postalCodeRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Postal code"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.keyboardType = UIKeyboardType.NumbersAndPunctuation
            }.configure {
                $0.placeholder = "Enter the postal code"
            }.onTextChanged {
                self.parentVC.postalCode = $0
        }
        let countryRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Country"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter the country"
            }.onTextChanged {
                self.parentVC.country = $0
        }
        let priceRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Price"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.keyboardType = UIKeyboardType.NumbersAndPunctuation
            }.configure {
                $0.placeholder = "Enter your price (example: 12.34 -> use a dot) "
            }.onTextChanged {
                self.parentVC.price = Float($0)
        }
        startDateRow = InlineDatePickerRowFormer<ProfileLabelCell>(instantiateType: .Nib(nibName: "ProfileLabelCell")) {
            $0.titleLabel.text = "Start date"
            }.configure {
                let date = NSDate()
                self.parentVC.startDate = date
                $0.date = date
            }.inlineCellSetup {
                self.startDatePicker = $0.datePicker
                $0.datePicker.datePickerMode = .DateAndTime
                $0.datePicker.minimumDate = $0.datePicker.date
                $0.datePicker.minuteInterval = 15
            }.displayTextFromDate {
                return String.mediumDateShortTime($0)
            }.onDateChanged {
                self.parentVC.startDate = $0
                if (self.startDatePicker.date > self.endDatePicker.date) {
                    let periodComponents = NSDateComponents()
                    periodComponents.minute = 15
                    
                    self.endDatePicker.date = NSCalendar.currentCalendar().dateByAddingComponents(periodComponents, toDate: self.startDatePicker.date, options: [])!
                    self.endDateRow.date = self.endDatePicker.date
                    self.parentVC.endDate = self.endDateRow.date
                }
        }
        endDateRow = InlineDatePickerRowFormer<ProfileLabelCell>(instantiateType: .Nib(nibName: "ProfileLabelCell")) {
            $0.titleLabel.text = "End date"
            }.configure {
                let periodComponents = NSDateComponents()
                periodComponents.minute = 15
                let date = NSCalendar.currentCalendar().dateByAddingComponents(periodComponents, toDate: self.startDateRow.date, options: [])!
                self.parentVC.endDate = date
                $0.date = date
            }.inlineCellSetup {
                self.endDatePicker = $0.datePicker
                $0.datePicker.datePickerMode = .DateAndTime
                $0.datePicker.minimumDate = $0.datePicker.date
                $0.datePicker.minuteInterval = 15
            }.displayTextFromDate {
                return String.mediumDateShortTime($0)
            }.onDateChanged {
                self.parentVC.endDate = $0
                if (self.endDatePicker.date < self.startDatePicker.date) {
                    let periodComponents = NSDateComponents()
                    periodComponents.minute = -15

                    self.startDatePicker.date = NSCalendar.currentCalendar().dateByAddingComponents(periodComponents, toDate: self.endDatePicker.date, options: [])!
                    self.startDateRow.date = self.startDatePicker.date
                    self.parentVC.startDate = self.startDateRow.date
                }
        }
        let creationDateRow = InlineDatePickerRowFormer<ProfileLabelCell>(instantiateType: .Nib(nibName: "ProfileLabelCell")) {
            $0.titleLabel.text = "Creation Date"
            }.configure {
                let date = NSDate()
                self.parentVC.creationDate = date
                $0.date = date
            }.inlineCellSetup {
                $0.datePicker.datePickerMode = .DateAndTime
                $0.datePicker.minimumDate = $0.datePicker.date
                $0.datePicker.maximumDate = $0.datePicker.date
            }.displayTextFromDate {
                return String.mediumDateShortTime($0)
            }.onDateChanged {
                self.parentVC.creationDate = $0
        }
        
        // Create Headers
        let createHeader: (String -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.viewHeight = 40
                    $0.text = text
            }
        }
        
        // Create SectionFormers
        let aboutSection = SectionFormer(rowFormer: categoryRow, titleRow, descriptionRow)
            .set(headerViewFormer: createHeader("About"))
        let imageSection = SectionFormer(rowFormer: pictureRow)
            .set(headerViewFormer: createHeader("Image"))
        let addressSection = SectionFormer(rowFormer: streetNumberRow, streetNameRow, cityRow, postalCodeRow, countryRow)
            .set(headerViewFormer: createHeader("Address"))
        let extraSection = SectionFormer(rowFormer: priceRow, startDateRow, endDateRow, creationDateRow)
            .set(headerViewFormer: createHeader("Additional"))
        
        former.append(sectionFormer: aboutSection, imageSection, addressSection, extraSection)
    }
    
    func configureConstraints() {
        if (!didSetupConstraints) {
            tableView.autoPinToTopLayoutGuideOfViewController(parentViewController!, withInset: 0.0)
            tableView.autoPinToBottomLayoutGuideOfViewController(parentViewController!, withInset: 0.0)
            tableView.autoPinEdgeToSuperviewEdge(.Leading)
            tableView.autoPinEdgeToSuperviewEdge(.Trailing)
            
            didSetupConstraints = true
        }
    }
}
