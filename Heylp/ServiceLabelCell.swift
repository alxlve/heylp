//
//  ServiceLabelCell.swift
//  Heylp
//
//  Created by Alexis Lavie on 01/04/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import Former

final class ServiceLabelCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}