//
//  SignUpViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 02/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    var username: String?
    var password: String?
    var email: String?
    var picture: UIImage?
    var firstName: String?
    var lastName: String?
    var gender: Gender?
    var birthday: NSDate?
    var streetNumber: String?
    var streetName: String?
    var city: String?
    var postalCode: String?
    var country: String?
    
    var myView: SignUpView! {
        return self.view as! SignUpView
    }
    
    init(_ coder: NSCoder? = nil) {
        if let coder = coder {
            super.init(coder: coder)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: {});
    }
    
    func save() {
        if (username != nil && password != nil && email != nil && picture != nil && firstName != nil && lastName != nil && gender != nil && birthday != nil && streetNumber != nil && streetName != nil && city != nil && postalCode != nil && country != nil) {
            
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.rightBarButtonItem = nil
            let address = Address(streetNumber: streetNumber!, streetName: streetName!, city: city!, postalCode: postalCode!, country: country!)
            address.updateCoordinates(completion: {
                let user = User(username: self.username!, password: self.password!, email: self.email!, firstName: self.firstName!, lastName: self.lastName!, gender: self.gender!, birthday: self.birthday!, address: address, phones: nil, acceptedServices: nil, proposedServices: nil)
                
                ModelAPI.sharedInstance.addUser(user, completionHandler: {
                    ModelAPI.sharedInstance.addUserPicture(user, picture: Picture(image: self.picture!), completion: {
                        self.dismissViewControllerAnimated(true, completion: nil);
                    })
                })
            })
        }
    }
    
    private func configure() {
        title = "SignUp"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: "cancel")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: "save")
    }
    
    internal func presentImagePicker() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .PhotoLibrary
        picker.allowsEditing = false
        presentViewController(picker, animated: true, completion: nil)
    }
    
    override func loadView() {
        super.loadView()
        view = SignUpView(frame: UIScreen.mainScreen().bounds)
        
        configure()
        myView.configure()
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        myView.configureConstraints()

        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        automaticallyAdjustsScrollViewInsets = false
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        view.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate
extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}

// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        self.picture = image
        myView.pictureRow.cellUpdate {
            $0.iconView.image = image
        }
    }
}
