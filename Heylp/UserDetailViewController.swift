//
//  UserDetailViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 23/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit

class UserDetailViewController: UIViewController {
    
    var mode: String?
    var user: User?
    var picture: Picture?
    var userId: UInt!
    
    var myView: UserDetailView! {
        return self.view as! UserDetailView
    }
    
    init(_ coder: NSCoder? = nil) {
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    func ownedServicesButtonTouchUpInside() {
        let servicesViewController = ServicesViewController()
        servicesViewController.mode = "user"
        
        servicesViewController.userId = userId
        servicesViewController.serviceStatus = nil
        navigationController?.pushViewController(servicesViewController, animated: true)
    }
    
    func suscribedServicesButtonTouchUpInside() {
        let servicesViewController = ServicesViewController()
        servicesViewController.mode = "user"
        
        servicesViewController.userId = userId
        servicesViewController.serviceStatus = ServiceStatus.Affected
        navigationController?.pushViewController(servicesViewController, animated: true)
    }
    
    func accomplishedServicesButtonTouchUpInside() {
        let servicesViewController = ServicesViewController()
        servicesViewController.mode = "user"
        
        servicesViewController.userId = userId
        servicesViewController.serviceStatus = ServiceStatus.Accomplished
        navigationController?.pushViewController(servicesViewController, animated: true)
    }
    
    private func configure() {
        if (mode == "self") {
            title = "Profile"
        } else {
            title = "\(user!.firstName) \(user!.lastName)"
        }
        myView.pictureImageView.image = picture?.image
        myView.firstNameLabel.text = user!.firstName
        myView.lastNameLabel.text = user!.lastName
        myView.usernameLabel.text = user!.username
        myView.genderLabel.text = user!.gender.description
        myView.emailLabel.text = user!.email
        myView.birthdayLabel.text = "Birthday: " + String.mediumDateNoTime(user!.birthday)
        if (user!.ownerNote == nil) {
            myView.ownerNoteCosmosView.rating = 0
            myView.ownerNoteCosmosView.text = "(0)"
        } else {
            myView.ownerNoteCosmosView.rating = Double(user!.ownerNote![0])
            myView.ownerNoteCosmosView.text = "(\(Int(user!.ownerNote![1])))"
        }
        if (user!.suscriberNote == nil) {
            myView.suscriberNoteCosmosView.rating = 0
            myView.suscriberNoteCosmosView.text = "(0)"
        } else {
            myView.suscriberNoteCosmosView.rating = Double(user!.suscriberNote![0])
            myView.suscriberNoteCosmosView.text = "(\(Int(user!.suscriberNote![1])))"
        }
        let userAnnotation = MKPointAnnotation()
        userAnnotation.coordinate = CLLocationCoordinate2DMake(user!.address.latitude, user!.address.longitude)
        userAnnotation.title = user!.address.toStringFirstPart()
        userAnnotation.subtitle = user!.address.toStringSecondPart()
        myView.changeUserAnnotation(userAnnotation)
    }
    
    override func loadView() {
        super.loadView()
        view = UserDetailView(frame: UIScreen.mainScreen().bounds)
        
        myView.configure()
        myView.setNeedsUpdateConstraints()
    }

    override func updateViewConstraints() {
        myView.configureConstraints()
        
        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Targets
        myView.refreshControl.addTarget(self, action: "reloadData", forControlEvents: UIControlEvents.ValueChanged)
        myView.ownedServicesButton.addTarget(self, action: "ownedServicesButtonTouchUpInside", forControlEvents: .TouchUpInside)
        myView.suscribedServicesButton.addTarget(self, action: "suscribedServicesButtonTouchUpInside", forControlEvents: .TouchUpInside)
        myView.accomplishedServicesButton.addTarget(self, action: "accomplishedServicesButtonTouchUpInside", forControlEvents: .TouchUpInside)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        myView.informationContainerView.roundCorners([.BottomLeft, .BottomRight], radius: 5.0)
        myView.mapView.roundCorners([.TopLeft, .TopRight, .BottomLeft, .BottomRight], radius: 5.0)
    }
    
    func reloadData() {
        ModelAPI.sharedInstance.getUser(userId, completionHandler: { (user) in
            self.user = user
            ModelAPI.sharedInstance.getUserPicture(self.userId, completion: { (picture) in
                self.picture = picture
                self.configure()
                dispatch_async(dispatch_get_main_queue()) {
                    self.myView.refreshControl.endRefreshing()
                }
            })
        })
    }
}
