//
//  Utilities.swift
//  Heylp
//
//  Created by Alexis Lavie on 05/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

final class Utilities {
    
    static func convertImageToBase64(image: UIImage) -> String {
//        let imageData = UIImagePNGRepresentation(image)
        let imageData = UIImageJPEGRepresentation(image, 0.70)
        
        return imageData!.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
    }
    
    static func convertBase64ToImage(base64String: String) -> UIImage {
        let decodedData = NSData(base64EncodedString: base64String, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)

        return UIImage(data: decodedData!)!
    }
}
