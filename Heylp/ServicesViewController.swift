//
//  ServicesViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 17/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit

class ServicesViewController: UIViewController {

    var mode: String!
    var services: [Service]!
    var serviceCategory: ServiceCategory?
    var userId: UInt!
    var serviceStatus: ServiceStatus?
    
    var myView: ServicesView! {
        return self.view as! ServicesView
    }

    init(_ coder: NSCoder? = nil) {
        self.services = [Service]()
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }

    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    private func configure() {
        if (mode == "category") {
            if (serviceCategory == nil) {
                title = "All"
            } else {
                title = serviceCategory!.description
            }
        } else if (mode == "user") {
            if (serviceStatus == nil) {
                title = "Proposed"
            } else {
                title = serviceStatus!.description
            }
        }
        title! += " (\(services.count))"
    }
    
    override func loadView() {
        super.loadView()
        view = ServicesView(frame: UIScreen.mainScreen().bounds)
        
        myView.configure()
        myView.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        myView.configureConstraints()
        
        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        
        // Delegates
        myView.tableView.delegate = self
        myView.tableView.dataSource = self
        
        // Targets
        myView.refreshControl.addTarget(self, action: "reloadData", forControlEvents: UIControlEvents.ValueChanged)
        
        let xib = UINib(nibName: "ServiceLabelCell", bundle: nil)
        myView.tableView.registerNib(xib, forCellReuseIdentifier: "ServiceLabelCell")
        myView.tableView.estimatedRowHeight = 70
        myView.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadData()
    }
    
    func reloadData() {
        if (mode == "category") {
            if (serviceCategory == nil) {
                ModelAPI.sharedInstance.getAllAvailableServices({ (services, _) in
                    self.services = services
                    self.configure()
                    self.myView.tableView.reloadData()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.myView.refreshControl.endRefreshing()
                    }
                })
            } else {
                ModelAPI.sharedInstance.getAllAvailableServices(serviceCategory!, completionHandler: { (services, _) in
                    self.services = services
                    self.configure()
                    self.myView.tableView.reloadData()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.myView.refreshControl.endRefreshing()
                    }
                })
            }
        } else if (mode == "user") {
            if (serviceStatus == nil) {
                ModelAPI.sharedInstance.getAllUserProposedService(userId, completion: { (services) in
                    self.services = services
                    self.configure()
                    self.myView.tableView.reloadData()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.myView.refreshControl.endRefreshing()
                    }
                })
            } else {
                if (serviceStatus! == ServiceStatus.Affected) {
                    ModelAPI.sharedInstance.getUserAllAffectedServices(userId, completion: { (services) in
                        self.services = services
                        self.configure()
                        self.myView.tableView.reloadData()
                        dispatch_async(dispatch_get_main_queue()) {
                            self.myView.refreshControl.endRefreshing()
                        }
                    })
                } else if (serviceStatus! == ServiceStatus.Accomplished) {
                    ModelAPI.sharedInstance.getUserAllAccomplishedServices(userId, completion: { (services) in
                        self.services = services
                        self.configure()
                        self.myView.tableView.reloadData()
                        dispatch_async(dispatch_get_main_queue()) {
                            self.myView.refreshControl.endRefreshing()
                        }
                    })
                }
            }
        }
    }
}

// MARK - UITableViewDelegate, UITableViewDataSource
extension ServicesViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services.count
    }

    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//            var cell = tableView.dequeueReusableCellWithIdentifier("UITableViewCell")
//            
//            if cell == nil {
//                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "UITableViewCell")
//            }
            
            var cell: ServiceLabelCell! = tableView.dequeueReusableCellWithIdentifier("ServiceLabelCell") as! ServiceLabelCell!
            
            if cell == nil {
                let xib = NSBundle.mainBundle().loadNibNamed("ServiceLabelCell", owner: self, options: nil)
                cell = xib.first as! ServiceLabelCell
            }
            
            let service = services[indexPath.row]
                
//            cell!.textLabel?.text = service.title
            cell!.title.text = service.title
            cell!.price.text = "\(service.price)€"
            cell!.date.text = String.mediumDateShortTime(service.creationDate)
            cell!.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.60)
            cell!.selectionStyle = UITableViewCellSelectionStyle.Default
            let customColorView = UIView()
            customColorView.backgroundColor = UIColor(red: 0, green: 0.478431, blue: 1.0, alpha: 0.5)
            cell!.selectedBackgroundView = customColorView
            
            return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let serviceDetailViewController = ServiceDetailViewController()
        
        serviceDetailViewController.userId = services[indexPath.row].owner
        serviceDetailViewController.serviceId = services[indexPath.row].id
        navigationController?.pushViewController(serviceDetailViewController, animated: true)
        
        myView.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
