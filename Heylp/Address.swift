//
//  Address.swift
//  Heylp
//
//  Created by Alexis Lavie on 04/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper
import MapKit

class Address: Mappable {

    var id: UInt?
    var owner: UInt?
    var streetNumber: String!
    var streetName: String!
    var city: String!
    var postalCode: String!
    var country: String!
    var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
    
    init(streetNumber: String, streetName: String, city: String, postalCode: String, country: String) {
        self.id = nil
        self.streetNumber = streetNumber
        self.streetName = streetName
        self.city = city
        self.postalCode = postalCode
        self.country = country
        self.latitude = 0.0
        self.longitude = 0.0
    }
    
    init() {}
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id             <- (map["id"], NoTransform(serialization: true))
        self.owner          <- (map["owner"], NoTransform(serialization: false))
        self.streetNumber   <- map["streetNumber"]
        self.streetName     <- map["streetName"]
        self.city           <- map["city"]
        self.postalCode     <- map["postalCode"]
        self.country        <- map["country"]
        self.latitude       <- map["latitude"]
        self.longitude       <- map["longitude"]
    }
    
    func toString() -> String {
        return "\(streetNumber), \(streetName), \(postalCode), \(city), \(country)"
    }
    
    func toStringFirstPart() -> String {
        return "\(streetNumber), \(streetName)"
    }
    
    func toStringSecondPart() -> String {
        return "\(postalCode), \(city), \(country)"
    }
    
    func updateCoordinates(completion completion: () -> ()) {
        self.forwardGeocoding(toString(), completion: { (success, coordinate) in
            if success {
                self.latitude = coordinate.latitude
                self.longitude = coordinate.longitude
            } else {
                // Something went wrong...
            }
            completion()
        })
    }
    
    func forwardGeocoding(address: String, completion: (Bool, CLLocationCoordinate2D!) -> ()) {
        let geoCoder = CLGeocoder()
        
        geoCoder.geocodeAddressString(address) { (placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            if error != nil {
                print(error?.localizedDescription)
                
                completion(false, nil)
            } else {
                if placemarks!.count > 0 {
                    let placemark = placemarks![0] as CLPlacemark
                    let location = placemark.location
                    
                    completion(true, location?.coordinate)
                }
            }
        }
    }
}
