//
//  ModelAPI.swift
//  Heylp
//
//  Created by Alexis Lavie on 06/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

class ModelAPI {
    
    static var sharedInstance = ModelAPI()
    
    private let phoneContextUrl = "phone"
    private let serviceContextUrl = "service"
    private let userContextUrl = "user"
    
    private let phoneAPI: PhoneRestAPI
    private let serviceAPI: ServiceRestAPI
    private let userAPI: UserRestAPI
    
    private init() {
        let baseUrl = "http://localhost:8080/rest"
        phoneAPI = PhoneRestAPI(baseUrl: baseUrl, userContextUrl: userContextUrl, contextUrl: phoneContextUrl)
        serviceAPI = ServiceRestAPI(baseUrl: baseUrl, userContextUrl: userContextUrl, contextUrl: serviceContextUrl)
        userAPI = UserRestAPI(baseUrl: baseUrl, contextUrl: userContextUrl)
    }
    
    func login(username: String, password: String) -> UInt? {
        return userAPI.login(username, password: password)
    }
    
    func getUser(id: UInt, completionHandler: ((User?) -> ())?) {
        userAPI.get(id, completionHandler: { (user) in
            completionHandler?(user)
        })
    }
    
    func getAllUsers(completionHandler completionHandler: (([User]) -> ())?) {
        userAPI.getAll(completionHandler: { (users) in
            completionHandler?(users)
        })
    }
    
    func addUser(user: User, completionHandler: (() -> ())?) {
        userAPI.add(user, completionHandler: { (id) in
            user.id = id
            completionHandler?()
        })
    }
    
    func getUserPicture(userId: UInt, completion: ((Picture?) -> ())?) {
        userAPI.getUserPicture(userId, completion: { (picture) in
            completion?(picture)
        })
    }
    
    func addUserPicture(user: User, picture: Picture, completion: (() -> ())?) {
        userAPI.addUserPicture(user.id!, object: picture, completionHandler: { (id) in
            picture.id = id
            completion?()
        })
    }
    
    func getUserProposedService(userId: UInt, serviceId: UInt, completion: ((Service?) -> ())?) {
        serviceAPI.userId = userId
        serviceAPI.get(serviceId, completionHandler: { (service) in
            completion?(service)
        })
    }
    
    func getAllUserProposedService(userId: UInt, completion: (([Service]?) -> ())?) {
        serviceAPI.userId = userId
        serviceAPI.getAll(completionHandler: { (services) in
            completion?(services)
        })
    }
    
    func addUserProposedService(user: User, service: Service, completionHandler: (() -> ())?) {
        serviceAPI.userId = user.id!
        serviceAPI.add(service, completionHandler: { (id) in
            service.id = id
            completionHandler?()
        })
    }
    
    func saveUserProposedService(userId: UInt, service: Service, completion: (() -> ())?) {
        serviceAPI.userId = userId
        serviceAPI.save(service.id!, object: service, completion: { _ in
            completion?()
        })
    }
    
    func getUserAllAffectedServices(userId: UInt, completion: (([Service]?) -> ())?) {
        serviceAPI.userId = userId
        serviceAPI.getUserAllAffectedServices(completion: { (services, _) in
            completion?(services)
        })
    }
    
    func getUserAllAccomplishedServices(userId: UInt, completion: (([Service]?) -> ())?) {
        serviceAPI.userId = userId
        serviceAPI.getUserAllAccomplishedServices(completion: { (services, _) in
            completion?(services)
        })
    }
    
    func getServicePicture(userId: UInt, serviceId: UInt, completion: ((Picture?) -> ())?) {
        serviceAPI.userId = userId
        serviceAPI.getServicePicture(serviceId, completion: { (picture) in
            completion?(picture)
        })
    }
    
    func addServicePicture(userId: UInt, service: Service, picture: Picture, completion: (() -> ())?) {
        serviceAPI.userId = userId
        serviceAPI.addServicePicture(service.id!, object: picture, completionHandler: { (id) in
            picture.id = id
            completion?()
        })
    }
    
    func getAllAvailableServices(completionHandler: (responseObject: [Service]?, error: NSError?) -> ()) {
         serviceAPI.getAllAvailableServices(completionHandler)
    }
    
    func getAllAvailableServices(serviceCategory: ServiceCategory, completionHandler: (responseObject: [Service]?, error: NSError?) -> ()) {
        serviceAPI.getAllAvailableServices(serviceCategory, completionHandler: completionHandler)
    }
}
