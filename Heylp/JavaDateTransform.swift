//
//  JavaDateTransform.swift
//  Heylp
//
//  Created by Alexis Lavie on 10/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

public class JavaDateTransform: TransformType {
    public typealias Object = NSDate
    public typealias JSON = Double
    
    var deserialization: Bool
    var serialization: Bool
    
    init(deserialization: Bool, serialization: Bool) {
        self.deserialization = deserialization
        self.serialization = serialization
    }
    
    convenience init() {
        self.init(deserialization: true, serialization: true)
    }
    
    convenience init(serialization: Bool) {
        self.init(deserialization: true, serialization: serialization)
    }
    
    public func transformFromJSON(value: AnyObject?) -> NSDate? {
        if (deserialization) {
            if let timeInt = value as? Double {
                return NSDate(timeIntervalSince1970: NSTimeInterval(timeInt) / 1000)
            }
            
            if let timeStr = value as? String {
                return NSDate(timeIntervalSince1970: NSTimeInterval(atof(timeStr) / 1000))
            }
        }
        
        return nil
    }
    
    public func transformToJSON(value: NSDate?) -> Double? {
        if (serialization) {
            if let date = value {
                return round(Double(date.timeIntervalSince1970 * 1000))
            }
        }
        
        return nil
    }
}