//
//  PhoneRestAPI.swift
//  Heylp
//
//  Created by Alexis Lavie on 15/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

class PhoneRestAPI: RestAPI<Phone> {
    
    internal var userContextUrl: String
    internal var userId: UInt {
        didSet {
            self.fullUrl = self.userContextUrl + "/" + String(self.userId) + "/" + self.contextUrl
        }
    }
    
    init(baseUrl: String, userContextUrl: String, contextUrl: String) {
        self.userContextUrl = userContextUrl
        self.userId = 0
        
        super.init()
        
        self.baseUrl = baseUrl
        self.contextUrl = contextUrl
    }
}
