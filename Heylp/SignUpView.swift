//
//  SignUpView.swift
//  Heylp
//
//  Created by Alexis Lavie on 12/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import Former

class SignUpView: UIView {
    
    var parentVC: SignUpViewController!
    var tableView: UITableView
    
    var didSetupConstraints = false

    var former: Former
    
    init(_ coder: NSCoder? = nil, _ frame: CGRect? = nil) {
        self.tableView = UITableView(frame: CGRect.zero, style: .Grouped)
        self.former = Former(tableView: tableView)
        
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(frame: frame!)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder, nil)
    }
    
    override required convenience init(frame: CGRect) {
        self.init(nil, frame)
    }
    
    lazy var pictureRow: LabelRowFormer<ProfileImageCell> = {
        LabelRowFormer<ProfileImageCell>(instantiateType: .Nib(nibName: "ProfileImageCell")) {
            $0.iconView.image = nil
            }.configure {
                $0.text = "Choose profile image from library"
                $0.rowHeight = 60
            }.onSelected { [weak self] _ in
                self?.former.deselect(true)
                self!.parentVC.presentImagePicker()
        }
    }()
    
    func configure() {
        parentVC = parentViewController as! SignUpViewController
        
        backgroundColor = .groupTableViewBackgroundColor()
        
        tableView.backgroundColor = .clearColor()
        insertSubview(tableView, atIndex: 0)
        
        // Create RowFormers
        let usernameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Username"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.keyboardType = UIKeyboardType.ASCIICapable
            $0.textField.autocapitalizationType = UITextAutocapitalizationType.None;
            $0.textField.autocorrectionType = UITextAutocorrectionType.No
            }.configure {
                $0.placeholder = "Enter your username"
            }.onTextChanged {
                self.parentVC.username = $0
        }
        let passwordRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Password"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.secureTextEntry = true
            }.configure {
                $0.placeholder = "Enter your password"
            }.onTextChanged {
                self.parentVC.password = $0
        }
        let emailRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Email"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.keyboardType = UIKeyboardType.EmailAddress
            $0.textField.autocapitalizationType = UITextAutocapitalizationType.None;
            $0.textField.autocorrectionType = UITextAutocorrectionType.No
            }.configure {
                $0.placeholder = "Enter your email"
            }.onTextChanged {
                self.parentVC.email = $0
        }
        let firstNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "First name"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter your first name"
            }.onTextChanged {
                self.parentVC.firstName = $0
        }
        let lastNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Last name"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter your last name"
            }.onTextChanged {
                self.parentVC.lastName = $0
        }
        let genderRow = InlinePickerRowFormer<ProfileLabelCell, Gender>(instantiateType: .Nib(nibName: "ProfileLabelCell")) {
            $0.titleLabel.text = "Gender"
            }.configure {
                self.parentVC.gender = Gender.values()[0]
                $0.pickerItems = Gender.values().map {
                    InlinePickerItem(title: $0.description, value: $0)
                }
            }.onValueChanged {
                self.parentVC.gender = $0.value
        }
        let birthdayRow = InlineDatePickerRowFormer<ProfileLabelCell>(instantiateType: .Nib(nibName: "ProfileLabelCell")) {
            $0.titleLabel.text = "Birthday"
            }.configure {
                let date = NSDate()
                self.parentVC.birthday = date
                $0.date = date
            }.inlineCellSetup {
                $0.datePicker.datePickerMode = .Date
            }.displayTextFromDate {
                return String.mediumDateNoTime($0)
            }.onDateChanged {
                self.parentVC.birthday = $0
        }
        let streetNumberRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Street number"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.keyboardType = UIKeyboardType.NumbersAndPunctuation
            }.configure {
                $0.placeholder = "Enter your street number"
            }.onTextChanged {
                self.parentVC.streetNumber = $0
        }
        let streetNameRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Street name"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.autocapitalizationType = UITextAutocapitalizationType.None;
            }.configure {
                $0.placeholder = "Enter your street name"
            }.onTextChanged {
                self.parentVC.streetName = $0
        }
        let cityRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "City"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter your city"
            }.onTextChanged {
                self.parentVC.city = $0
        }
        let postalCodeRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Postal code"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            $0.textField.keyboardType = UIKeyboardType.NumbersAndPunctuation
            }.configure {
                $0.placeholder = "Enter your postal code"
            }.onTextChanged {
                self.parentVC.postalCode = $0
        }
        let countryRow = TextFieldRowFormer<ProfileFieldCell>(instantiateType: .Nib(nibName: "ProfileFieldCell")) {
            $0.titleLabel.text = "Country"
            $0.textField.returnKeyType = UIReturnKeyType.Next
            }.configure {
                $0.placeholder = "Enter your country"
            }.onTextChanged {
                self.parentVC.country = $0
        }
        
        // Create Headers
        let createHeader: (String -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>()
                .configure {
                    $0.viewHeight = 40
                    $0.text = text
            }
        }
        
        // Create SectionFormers
        let imageSection = SectionFormer(rowFormer: pictureRow)
            .set(headerViewFormer: createHeader("Profile Image"))
        let aboutSection = SectionFormer(rowFormer: usernameRow, passwordRow, emailRow, firstNameRow, lastNameRow, genderRow, birthdayRow)
            .set(headerViewFormer: createHeader("About"))
        let addressSection = SectionFormer(rowFormer: streetNumberRow, streetNameRow, cityRow, postalCodeRow, countryRow)
            .set(headerViewFormer: createHeader("Address"))
        
        former.append(sectionFormer: imageSection, aboutSection, addressSection)
    }
    
    func configureConstraints() {
        if (!didSetupConstraints) {
            tableView.autoPinToTopLayoutGuideOfViewController(parentViewController!, withInset: 0.0)
            tableView.autoPinToBottomLayoutGuideOfViewController(parentViewController!, withInset: 0.0)
            tableView.autoPinEdgeToSuperviewEdge(.Leading)
            tableView.autoPinEdgeToSuperviewEdge(.Trailing)
            
            didSetupConstraints = true
        }
    }
}
