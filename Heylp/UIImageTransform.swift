//
//  UIImageTransform.swift
//  Heylp
//
//  Created by Alexis Lavie on 05/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import ObjectMapper

class UIImageTransform: TransformType {
    typealias Object = UIImage
    typealias JSON = String
    
    var deserialization: Bool
    var serialization: Bool
    
    init(deserialization: Bool, serialization: Bool) {
        self.deserialization = deserialization
        self.serialization = serialization
    }
    
    convenience init() {
        self.init(deserialization: true, serialization: true)
    }
    
    convenience init(serialization: Bool) {
        self.init(deserialization: true, serialization: serialization)
    }
    
    func transformFromJSON(value: AnyObject?) -> UIImage? {
        if (deserialization) {
            if let base64String = value as? String {
                return Utilities.convertBase64ToImage(base64String)
            }
        }
        
        return nil
    }
    
    func transformToJSON(value: UIImage?) -> String? {
        if (serialization) {
            if let image = value {
                return Utilities.convertImageToBase64(image)
            }
        }
        
        return nil
    }
}
