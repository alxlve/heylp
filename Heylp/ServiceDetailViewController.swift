//
//  ServiceDetailViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 21/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit

class ServiceDetailViewController: UIViewController {
    
    var service: Service?
    var picture: Picture?
    var serviceId: UInt!
    var user: User?
    var userId: UInt!
    var suscriber: User?
    var action: String!
    
    weak var myView: ServiceDetailView! {
        return self.view as! ServiceDetailView
    }

    init(_ coder: NSCoder? = nil) {
        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    private func save(completion completion: (() -> ())?) {
        myView.actionButton.enabled = false
        myView.ownerNoteCosmosView.userInteractionEnabled = false
        myView.suscriberNoteCosmosView.userInteractionEnabled = false
        ModelAPI.sharedInstance.saveUserProposedService(userId, service: service!, completion: {
            self.myView.actionButton.enabled = true
            self.configureActionButton()
            self.configureNotes()
            completion?()
        })
    }
    
    func actionButtonTouchUpInside() {
        if (self.action == "suscribe") {
            ModelAPI.sharedInstance.getUserProposedService(userId, serviceId: serviceId, completion: { (service) in
                let checkService = service
                if (checkService?.suscriber == nil) {
                    self.service!.suscriber = App.sharedInstance.getConnectedUser()?.id
                    self.service!.status = ServiceStatus.Affected
                    self.save(completion: {
                        self.reloadSuscriber(completion: {
                            self.configureSuscriberButton()
                        })
                    })
                } else {
                    self.configureSuscriberButton()
                    
                    let alertController = UIAlertController(title: "Already suscribed", message: "Someone has already suscribed the service before you !", preferredStyle: .Alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            })
        } else if (self.action == "accomplish") {
            self.service!.status = ServiceStatus.Accomplished
            self.save(completion: nil)
        }
        self.myView.statusLabel.text = service!.status.description
    }

    func ownerButtonTouchUpInside() {
        let userDetailViewController = UserDetailViewController()
        
        userDetailViewController.userId = service!.owner
        navigationController?.pushViewController(userDetailViewController, animated: true)
    }
    
    func suscriberButtonTouchUpInside() {
        let userDetailViewController = UserDetailViewController()
        
        userDetailViewController.userId = suscriber!.id
        navigationController?.pushViewController(userDetailViewController, animated: true)
    }
    
    func didFinishTouchingOwnerNoteCosmos(rating: Double) {
        service!.ownerNote = Float(rating)
        save(completion: nil)
    }
    
    func didFinishTouchingSuscriberNoteCosmos(rating: Double) {
        service!.suscriberNote = Float(rating)
        save(completion: nil)
    }
    
    private func configureActionButton() {
        myView.actionButton.enabled = false
        if (App.sharedInstance.getConnectedUser() != nil) {
            if (App.sharedInstance.getConnectedUser()!.id != service!.owner) {
                myView.actionButton.enabled = true
                if (service!.status == ServiceStatus.Available) {
                    myView.actionButton.setTitle("Suscribe", forState: .Normal)
                    action = "suscribe"
                } else if (service!.status == ServiceStatus.Affected) {
                    myView.actionButton.setTitle("Mark as accomplished", forState: .Normal)
                    action = "accomplish"
                } else {
                    myView.actionButton.enabled = false
                }
            }
        }
    }
    
    private func configureNotes() {
        myView.ownerNoteCosmosView.didFinishTouchingCosmos = didFinishTouchingOwnerNoteCosmos
        myView.suscriberNoteCosmosView.didFinishTouchingCosmos = didFinishTouchingSuscriberNoteCosmos
        
        myView.ownerNoteCosmosView.userInteractionEnabled = false
        myView.suscriberNoteCosmosView.userInteractionEnabled = false
        if (App.sharedInstance.getConnectedUser() != nil) {
            if (service!.status == ServiceStatus.Accomplished) {
                if (App.sharedInstance.getConnectedUser()!.id == service!.suscriber) {
                    myView.ownerNoteCosmosView.userInteractionEnabled = true
                }
                if (App.sharedInstance.getConnectedUser()!.id == service!.owner) {
                    myView.suscriberNoteCosmosView.userInteractionEnabled = true
                }
            }
        }
        if (service!.ownerNote == nil) {
            myView.ownerNoteCosmosView.rating = 0
            myView.ownerNoteCosmosView.text = "[No value]"
        } else {
            myView.ownerNoteCosmosView.rating = Double(service!.ownerNote!)
            myView.ownerNoteCosmosView.text = "[= \(service!.ownerNote!)]"
        }
        if (service!.suscriberNote == nil) {
            myView.suscriberNoteCosmosView.rating = 0
            myView.suscriberNoteCosmosView.text = "[No value]"
        } else {
            myView.suscriberNoteCosmosView.rating = Double(service!.suscriberNote!)
            myView.suscriberNoteCosmosView.text = "[= \(service!.suscriberNote!)]"
        }
    }
    
    private func configureSuscriberButton() {
        if (suscriber == nil) {
            myView.suscriberButton.enabled = false
            myView.suscriberUsernameLabel.text = "[No suscriber]"
        } else {
            myView.suscriberButton.enabled = true
            myView.suscriberUsernameLabel.text = "\(suscriber!.username)"
        }
    }
    
    private func configure() {
        title = service!.title
        myView.pictureImageView.image = picture?.image
        myView.titleLabel.text = service!.title
        myView.categoryLabel.text = service!.category.description
        myView.priceLabel.text = String(service!.price) + "€"
        myView.descriptionLabel.text = service!.description
        myView.startDateLabel.text = String.mediumDateShortTime(service!.startDate)
        myView.endDateLabel.text = String.mediumDateShortTime(service!.endDate)
        myView.creationDateLabel.text = String.mediumDateShortTime(service!.creationDate)
        myView.statusLabel.text = service!.status.description
        myView.ownerUsernameLabel.text = "\(user!.username)"
        configureActionButton()
        configureNotes()
        
        let serviceAnnotation = MKPointAnnotation()
        serviceAnnotation.coordinate = CLLocationCoordinate2DMake(service!.address.latitude, service!.address.longitude)
        serviceAnnotation.title = service!.address.toStringFirstPart()
        serviceAnnotation.subtitle = service!.address.toStringSecondPart()
        myView.changeServiceAnnotation(serviceAnnotation)
    }
    
    override func loadView() {
        super.loadView()
        view = ServiceDetailView(frame: UIScreen.mainScreen().bounds)
        
        myView.configure()
        myView.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        myView.configureConstraints()
        
        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Targets
        myView.refreshControl.addTarget(self, action: "reloadData", forControlEvents: UIControlEvents.ValueChanged)
        myView.actionButton.addTarget(self, action: "actionButtonTouchUpInside", forControlEvents: UIControlEvents.TouchUpInside)
        myView.ownerButton.addTarget(self, action: "ownerButtonTouchUpInside", forControlEvents: UIControlEvents.TouchUpInside)
        myView.suscriberButton.addTarget(self, action: "suscriberButtonTouchUpInside", forControlEvents: UIControlEvents.TouchUpInside)
        
//        let forceRotationHack = UIViewController()
//        parentViewController!.presentViewController(forceRotationHack, animated: false, completion: nil)
//        forceRotationHack.dismissViewControllerAnimated(false, completion: nil)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        let rootViewController = appDelegate.window!.rootViewController
//        let window = appDelegate.window
//        appDelegate.window?.rootViewController = self
//        appDelegate.window?.rootViewController = rootViewController

        reloadData()
    }
    
    deinit {
        self.view.removeFromSuperview()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        myView.informationContainerView.roundCorners([.BottomLeft, .BottomRight], radius: 5.0)
        myView.mapView.roundCorners([.TopLeft, .TopRight, .BottomLeft, .BottomRight], radius: 5.0)
    }
    
    func reloadSuscriber(completion completion: (() -> ())?) {
        ModelAPI.sharedInstance.getUser(self.service!.suscriber!, completionHandler: { (user) in
            self.suscriber = user
            completion?()
        })
    }
    
    func reloadData() {
        ModelAPI.sharedInstance.getUserProposedService(userId, serviceId: serviceId, completion: { (service) in
            self.service = service
            ModelAPI.sharedInstance.getUser(self.userId, completionHandler: { (user) in
                self.user = user
                ModelAPI.sharedInstance.getServicePicture(self.userId, serviceId: self.serviceId, completion: { (picture) in
                    self.picture = picture
                    if (self.service!.suscriber != nil) {
                        self.reloadSuscriber(completion: {
                            self.configure()
                            self.configureSuscriberButton()
                            dispatch_async(dispatch_get_main_queue()) {
                                self.myView.refreshControl.endRefreshing()
                            }
                        })
                    } else {
                        self.configure()
                        self.configureSuscriberButton()
                        dispatch_async(dispatch_get_main_queue()) {
                            self.myView.refreshControl.endRefreshing()
                        }
                    }
                })
            })
        })
    }
    
//    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        return UIInterfaceOrientationMask.Portrait
//    }
}
