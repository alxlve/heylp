//
//  MapViewController.swift
//  Heylp
//
//  Created by Alexis Lavie on 21/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
        
    var myView: MapView! {
        return self.view as! MapView
    }
    
    var manager: CLLocationManager
    
    init(_ coder: NSCoder? = nil) {
        self.manager = CLLocationManager()

        if let _ = coder {
            super.init(coder: coder!)!
        } else {
            super.init(nibName: nil, bundle:nil)
        }
    }
    
    required convenience init(coder: NSCoder) {
        self.init(coder)
    }
    
    private func configure() {
        title = "Map"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: "reloadData")
    }
    
    override func loadView() {
        super.loadView()
        view = MapView(frame: UIScreen.mainScreen().bounds)
        
        // Delegates
        myView.mapView.delegate = self
        
        configure()
        myView.configure()
        myView.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        myView.configureConstraints()
        
        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Delegates
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadData()
        manager.startUpdatingLocation()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        manager.stopUpdatingLocation()
    }
    
    func reloadData() {
        myView.removeServiceAnnotations()
        myView.removeUserAnnotations()
        
        ModelAPI.sharedInstance.getAllAvailableServices({ (services, _) in
            for service in services! {
                let annotation = ServicePointAnnotation(service: service)
                annotation.coordinate = CLLocationCoordinate2DMake(service.address.latitude, service.address.longitude)
                annotation.title = service.title
                annotation.subtitle = service.description
                self.myView.addServiceAnnotation(annotation)
            }
        })
        ModelAPI.sharedInstance.getAllUsers(completionHandler: { (users) in
            for user in users {
                let annotation = UserPointAnnotation(user: user)
                annotation.coordinate = CLLocationCoordinate2DMake(user.address.latitude, user.address.longitude)
                annotation.title = "\(user.firstName) \(user.lastName)"
                annotation.subtitle = user.username
                self.myView.addUserAnnotation(annotation)
            }
        })
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        if annotation.isKindOfClass(ServicePointAnnotation.self) {
            let identifier = "service"
            let servicePointAnnotation = annotation as! ServicePointAnnotation
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
            
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                
                annotationView?.pinTintColor = servicePointAnnotation.pinColor
                annotationView?.leftCalloutAccessoryView = UIButton(type: .DetailDisclosure)
            } else {
                annotationView?.annotation = annotation
            }
            
            return annotationView
        }
        
        if annotation.isKindOfClass(UserPointAnnotation.self) {
            let identifier = "user"
            let userPointAnnotation = annotation as! UserPointAnnotation
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
            
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                
                annotationView?.pinTintColor = userPointAnnotation.pinColor
                if (userPointAnnotation.user != nil) {
                    annotationView?.leftCalloutAccessoryView = UIButton(type: .ContactAdd)
                }
            } else {
                annotationView?.annotation = annotation
            }
            
            return annotationView
        }
        
        return nil
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let annotation = view.annotation!
        
        if (annotation.isKindOfClass(ServicePointAnnotation.self)) {
            let servicePointAnnotation = annotation as! ServicePointAnnotation
            let serviceDetailViewController = ServiceDetailViewController()
            
            serviceDetailViewController.userId = servicePointAnnotation.service.owner
            serviceDetailViewController.serviceId = servicePointAnnotation.service.id
            navigationController?.pushViewController(serviceDetailViewController, animated: true)
        }
        if (annotation.isKindOfClass(UserPointAnnotation.self)) {
            let userPointAnnotation = annotation as! UserPointAnnotation
            let userDetailViewController = UserDetailViewController()
            
            userDetailViewController.userId = userPointAnnotation.user?.id
            navigationController?.pushViewController(userDetailViewController, animated: true)
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0]
        let latitude: CLLocationDegrees = userLocation.coordinate.latitude
        let longitude: CLLocationDegrees = userLocation.coordinate.longitude
        let latitudeDelta: CLLocationDegrees = 0.025
        let longitudeDelta: CLLocationDegrees = 0.025
        let span: MKCoordinateSpan = MKCoordinateSpanMake(latitudeDelta, longitudeDelta)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
        myView.mapView.setRegion(region, animated: false)

        myView.currentAnnotation.coordinate = userLocation.coordinate
        
        // Au choix :
        manager.stopUpdatingLocation()
    }
}
