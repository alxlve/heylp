//
//  UserRestAPI.swift
//  Heylp
//
//  Created by Alexis Lavie on 15/03/2016.
//  Copyright © 2016 Alexis Lavie. All rights reserved.
//

import Alamofire
import ObjectMapper

class UserRestAPI: RestAPI<User> {
    
    init(baseUrl: String, contextUrl: String) {        
        super.init()
        
        self.baseUrl = baseUrl
        self.contextUrl = contextUrl
        self.fullUrl = self.contextUrl
    }
    
    func login(username: String, password: String) -> UInt? {
        let parameters: [String: AnyObject] = [
            "username": username,
            "password": password
        ]
        var id: UInt?
        
        let semaphore = dispatch_semaphore_create(0)
        Alamofire.request(.POST, baseUrl + "/_login/login", parameters: parameters, encoding: .JSON)
            .responseString { response in
                switch response.result {
                    case .Success(let data):
                        id = UInt(data)
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                }
                dispatch_semaphore_signal(semaphore)
            }
        
        while dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW) != 0 {
            NSRunLoop.currentRunLoop().runMode(NSDefaultRunLoopMode, beforeDate: NSDate(timeIntervalSinceNow: 10))
        }
        
        return id
    }
    
    func getUserPicture(userId: UInt, completion: (responseObject: Picture?) -> ()) {
        Alamofire.request(.GET, fullUrl + "/\(userId)" + "/picture").responseObject { (response: Response<Picture, NSError>) in
            let object = response.result.value
            completion(responseObject: object)
        }
    }
    
    func addUserPicture(userId: UInt, object: Picture, completionHandler: (responseObject: UInt?) -> ()) {
        //        let request = NSMutableURLRequest(URL: NSURL(string: fullUrl)!)
        //        request.HTTPMethod = "POST"
        //        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let parameters = Mapper().toJSON(object)
        
        Alamofire.request(.POST, fullUrl + "/\(userId)" + "/picture", parameters: parameters, encoding: .JSON)
            .responseString { response in
                switch response.result {
                case .Success(let value):
                    completionHandler(responseObject: UInt(value))
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
    }
}
