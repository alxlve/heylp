README

### PROJETS

heylp = iOS

heylp-backend = backend Java (Jackson + Jersey + Spring + JPA + Hibernate + H2)


### FOURNIS

heylp.zip = iOS
heylp-backend.zip = Serveur

apps.zip = Packages d'installation
images.zip = Les images à importer si vous voulez rejouer ma soutenance avec mes images. (Utilité optionnelle.)
videos.zip = Les vidéos pour le déploiment de l'application.
README.txt


### INSTALLATION

Voir les vidéos, je fais cela car j'ai peur que juste avec du texte vous manquiez des choses (ou que je manque d'en préciser).
Il n'y a pas de son (je ne peux en mettre), mais on voit clairement ce que j'y fais.
Respectez bien à la lettre les configurations définies.
Si vous avez des problèmes n'hésitez pas à m'envoyer un courriel (alexis.lavie@etu.univ-orleans.fr ou lavie.alexis@gmail.com de préférence).
Les packages vous sont fournis.

Voici l'ordre :

1 - Cloner les dépôts (ou dézipper les projets).
	git clone https://Alexislavie@bitbucket.org/Alexislavie/heylp.git
	git clone https://Alexislavie@bitbucket.org/Alexislavie/heylp-backend.git

2 - Installer Tomcat 8 + IntelliJIDEA (+ INSTALLER LE JDK 8)

3 - Configurer IntelliJIDEA + Configuration de Tomcat

4 - Copier le backup de la base de données (utiliser soit presentation-start soit presentation-end dans heylp-backend/db_backup)

5 - Premier lancement de Tomcat et résolution des problèmes

6 - Ouvrir la base de données dans IntelliJIDEA

7 - Après édition de la base de données, veuillez TOUJOURS fermer la connection, ou Hibernate ne pourra plus interroger la base, et ne renverra pas les données à l'application iOS.
	Pour ouvrir / lancer le projet iOS, veuillez impérativement ouvrir le .xcworkspace et non le .xcodeproj (utilisation de CocoaPod).

8 - Tutoriel pour lancer plusieurs simulateurs.
	En premier lieu déployer l'application avec Xcode sur chacun des terminaux souhaités.

	xcrun instruments -w help
	open -n /Applications/Xcode.app/Contents/Developer/Applications/Simulator.app/ --args -CurrentDeviceUDID 'REMPLACER_PAR_UDID'


### RÉALISÉ dans le backend dans l'application iOS :

 - Service REST/JSON, j'ai codé seulement les méthodes dont j'avais besoin, cela fonctionne bien, il reste des fonctions (l'en-tête) que je pensais utiliser mais que je n'ai pas utilisé, je pourrais les supprimer mais ce n'est pas important.
 - Toutes les couches inférieures sont propres et nettoyées, que ce soit l'interface du modèle (Spring), la dao, etc.
 - Testez avec votre navigateur : http://localhost:8080/rest/user -> récupère tous les utilisateurs.
 - Le modèle est annoté pour la persistance et la serialization / deserialization JSON.


### NON RÉALISÉ :

 - Gestion / propagation complète des erreurs à travers les couches (je me suis concentré sur le focntionnel).
 - Validation des champs (il faut être un utilisateur discipliné).
 - À la création, je ne vérifie pas si username est déjà dans la base (pour se connecter il y a vérification, si mauvais couple username, password il y a un avertissement affiché).


### RÉALISÉ dans l'application iOS :

 Je ne détaille pas ce qui a été présenté en soutenance [noté [S]] (c'est ce que vous m'avez dit...) mais je cite ce que j'ai exploré / appris,
 ce n'est pas dans un ordre précis, je liste :
 - Côté fonctionnel minimal demandé dans le sujet OK, je ne reviens pas dessus, c'est entièrement présenté à la soutenance. [S]
 - Animation maîtrisée (c'est-à-dire cycle de vie de la vue, mise en arrière plan, etc.) // LoginViewController [S]
 - Vue très peaufinée, gestion manuelle du clavier, utilisation d'UIEffectView. // LoginViewController
 - Autres vues un peu moins peaufinées, mais quand même bien travaillées.
 - Architecture MVC complète, séparation des couches lien + ou - 1, la vue ne connaît pas le modèle, View séparée de son Controller. [S]
 - Gestion optimisée de la mémoire pour une vue, UIImage sans mise en cache. // LoginViewController [S]
 - Champs des formulaires reliés entre eux, touche Next du clavier. [S]
 - Clavier bien géré (UITextField toujours au-dessus) sur toutes les vues, voir IQKeyboardManager.
 - Les objets du modèle sont mappés pour la serialization / déserialization, voir ObjectMapper. [S]
 - Requêtes réseau en asynchrone, voir Alamofire.
 - Contraintes maîtrisées, sans constantes de positionnement !, tout est relatif, rotation de toutes les vues avec le même code. Bloqué en portrait pour LoginViewController à cause de l'animation. [S]
 - Un controller ne connaît pas l'implémentation du modèle, il n'utilise que l'interface. [S]
 - Plusieurs approches pour les vues, tout en code ou storyboard (fait dans le td noté, vue du storyboard instanciée par le code, pas de segue) ou utilisation d'un .xib (comme le storyboard mais ne contient qu'une UNIQUE vue, attention avant l'extension du fichier était .nib. Les fonctions pour ce type de vue ont gardés par soucis de compatibilité le nom Nib si vous regardez la doc.) -> me permets de comprendre les avantages et inconvénients de chaque méthode.
 - CustomCellView avec des .xib .
 - Refresh de toutes les vues d'affichage de valeurs, UIRefreshControl. [S]
 - Vrai backend, donc partage des données entre périphériques. [S]
 - Utilisation du réseau optimisée, je ne télécharge l'image d'un User ou Service qu'au besoin. [S]
 - Gestion complète des notes, moyennes calculées à la volée en demandant un User par le serveur. [S]
 - Deux utilisateurs A et B sont sur le même service avec ServiceDetailViewController, B clique sur Suscribe avant A, A peut toujours cliquer, il clique mais se prend un UIAlertController l'avertissant qu'il est déjà pris -> exemple de résolution d'un problème de concurrence de modification des données.
 - Push "infini" des vues dans les NavigationController. [S]
 - Gestion d'images, convertit en JPG avec compression pour réduire le poids sur la transmission et le stockage.
 - Gestion des serializations / déserializations des objets complexes, fonctions de mapping (exemple avec les Date converties en timestamp). [S]
 - Sérialization / déserialization automatique des objets avec AlamofireOjectMapper, quand je reçois / envois un objet, celui-ci est automatiquement convertit.
 - Un controller peut charger différement les données selon l'instantiation (voir reloadData() de ServicesViewController cette vue est pushée par BrowseCategoryViewController et par UserDetailViewController de deux manières). [S]
 - Mode invité géré avec le même code que pour un utilisateur connecté [S]
 - Changer de Tab sur le TabBarController, fait unpop toutes les vues de la NavigationController sur laquelle on était auparavant. (Fonctionne aussi si on clique sur la Tab actuelle). [S]
 - Carte avec CustomPointAnnotation (couleurs différentes, AccessoryView) [S]
 - Positionnement longitude / latitude d'une adresse stockée avec l'adresse. Pourquoi la stocker ? Car je la génère après la création d'une adresse. Pourquoi pas à la volée ? Car en affichant la carte je devrais le faire pour tout ce qui est affiché (User + Service) et l'API Apple autorise un nombre de requêtes limitées par minute par ip, nombre très faible) donc je les stocke. Il faut simplement penser à updateCoordinates() si on met à jour l'adresse.
 - Dans la vue UserDetailView, j'ai expérimenté les ContentCompressionResistancePriority et ContentHuggingPriority, en soit la hauteur de mon image est définie par la hauteur des champs à sa droite, sa largeur par sa hauteur, cela fait beaucoup de contraintes inter-dépendentes d'où l'usage nécessaire des priorités. Si vous enlevez les priorités vous verrez cela fait n'importe quoi.


### BIZARRERIES :

 - Si la Map n'affiche votre position au bon endroit, c'est le simulateur qui fait n'importe quoi, cliquez sur (Debug > Location > Apple, puis Debug > Location > Custom Location...) x2 deux fois, puis forcez l'application à refresh la position en changeant de tab et revenir sur la Map (j'ai défini qu'une fois qu'on trouve la position on ne la mets plus à jour en restant sur la Map, voir dernière instruction de la classe MapViewController).
 - Faites attention à l'asynchrone, vous changez des valeurs sur un objet, sur un autre périphérique rafraichissez la vue de l'objet, il se peut que vous ayez les anciennes valeurs, C'EST NORMAL. Le serveur n'a peut-être pas eu le temps d'enregistrer la requête précédente de modification. (Attendez quelques secondes avant de rafraîchir.)


### ASTUCES :

 - Ajouter une image dans la gallerie du simulateur ? Ouvrir l'applications Photos d'iOS, glisser / déposer à partir du Finder une image (une par une, autrement ça ne fonctionne pas).


### CONCLUSION

Je souhaitais apprendre à fond le développement pour iOS, j'ai voulu faire une "vraie application", la base d'un cas réel, et c'est pour cela que j'ai fais un backend JSON, cela représente beaucoup de travail mais je pourrai facilement si je dois créer une application iOS me baser sur tout ce que j'ai déjà appris / maîtrisé, codé. J'ai un maximum préféré explorer des choses différentes, apprendre que de faire de nouvelles vues.
Je suis heureux du résultat. Il pourra plus tard être amélioré pour servir de base à une autre application, en entreprise ou dans un projet personnel.

Alexis Lavie
